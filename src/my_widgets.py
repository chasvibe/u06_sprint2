from PyQt5 import QtCore, QtGui, QtWidgets


class newAuction(QtWidgets.QFrame):
    def __init__(self, *args, **kwargs):
        QtWidgets.QFrame.__init__(self, *args, **kwargs)
        sizePolicy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Preferred
        )
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.sizePolicy().hasHeightForWidth())
        self.setSizePolicy(sizePolicy)
        self.setMinimumSize(QtCore.QSize(720, 250))
        self.setMaximumSize(QtCore.QSize(16777215, 250))
        self.setStyleSheet(
            """QFrame {background-color: #cfe1b9;
            border-width: 1px;
            border-style: solid;
            border-color: #718355;
            border-radius: 5px;}"""
        )
        self.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.setFrameShadow(QtWidgets.QFrame.Raised)
        self.setObjectName("iAuction")
        self.gridLayout_18 = QtWidgets.QGridLayout(self)
        self.gridLayout_18.setContentsMargins(-1, 12, -1, -1)
        self.gridLayout_18.setHorizontalSpacing(30)
        self.gridLayout_18.setObjectName("gridLayout_18")
        self.address_label_2 = QtWidgets.QLabel(self)
        self.address_label_2.setStyleSheet(
            "QLabel {color: #4a4a48; border: 0px;}"
        )
        self.address_label_2.setObjectName("address_label_2")
        self.gridLayout_18.addWidget(self.address_label_2, 1, 0, 1, 1)
        self.date_created_label = QtWidgets.QLabel(self)
        self.date_created_label.setStyleSheet(
            "QLabel {color: #4a4a48; border: 0px;}"
        )
        self.date_created_label.setObjectName("date_created_label")
        self.gridLayout_18.addWidget(self.date_created_label, 2, 0, 1, 1)
        self.end_date_label_2 = QtWidgets.QLabel(self)
        self.end_date_label_2.setStyleSheet(
            "QLabel {color: #4a4a48; border: 0px;}"
        )
        self.end_date_label_2.setObjectName("end_date_label_2")
        self.gridLayout_18.addWidget(self.end_date_label_2, 3, 0, 1, 1)
        self.auctionID_label = QtWidgets.QLabel(self)
        self.auctionID_label.setStyleSheet(
            "QLabel {color: #4a4a48; border: 0px;}"
        )
        self.auctionID_label.setObjectName("auctionID_label")
        self.gridLayout_18.addWidget(self.auctionID_label, 0, 0, 1, 1)
        self.price_label_2 = QtWidgets.QLabel(self)
        self.price_label_2.setStyleSheet(
            "QLabel {color: #4a4a48; border: 0px;}"
        )
        self.price_label_2.setObjectName("price_label_2")
        self.gridLayout_18.addWidget(self.price_label_2, 4, 0, 1, 1)
        self.kvm_label_2 = QtWidgets.QLabel(self)
        self.kvm_label_2.setStyleSheet(
            "QLabel {color: #4a4a48; border: 0px;}"
        )
        self.kvm_label_2.setObjectName("kvm_label_2")
        self.gridLayout_18.addWidget(self.kvm_label_2, 5, 0, 1, 1)
        self.iLabels = QtWidgets.QWidget(self)
        sizePolicy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Preferred
        )
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            self.iLabels.sizePolicy().hasHeightForWidth()
        )
        self.iLabels.setSizePolicy(sizePolicy)
        self.iLabels.setMinimumSize(QtCore.QSize(550, 226))
        self.iLabels.setMaximumSize(QtCore.QSize(16777215, 226))
        self.iLabels.setStyleSheet("border-radius: 5px;")
        self.iLabels.setObjectName("iLabels")
        self.gridLayout_20 = QtWidgets.QGridLayout(self.iLabels)
        self.gridLayout_20.setContentsMargins(-1, 0, -1, 0)
        self.gridLayout_20.setObjectName("gridLayout_20")
        self.price_iLabel = QtWidgets.QLabel(self.iLabels)
        self.price_iLabel.setStyleSheet(
            """QLabel {background-color: #e9f5db; color: #4a4a48;
            border: 0px;}"""
        )
        self.price_iLabel.setObjectName("price_iLabel")
        self.gridLayout_20.addWidget(self.price_iLabel, 4, 0, 1, 1)
        self.address_iLabel = QtWidgets.QLabel(self.iLabels)
        self.address_iLabel.setStyleSheet(
            """QLabel {background-color: #e9f5db; color: #4a4a48;
            border: 0px;}"""
        )
        self.address_iLabel.setObjectName("address_iLabel")
        self.gridLayout_20.addWidget(self.address_iLabel, 1, 0, 1, 1)
        self.kvm_iLabel = QtWidgets.QLabel(self.iLabels)
        self.kvm_iLabel.setStyleSheet(
            """QLabel {background-color: #e9f5db; color: #4a4a48;
            border: 0px;}"""
        )
        self.kvm_iLabel.setObjectName("kvm_iLabel")
        self.gridLayout_20.addWidget(self.kvm_iLabel, 5, 0, 1, 1)
        self.date_created_iLabel = QtWidgets.QLabel(self.iLabels)
        self.date_created_iLabel.setStyleSheet(
            """QLabel {background-color: #e9f5db; color: #4a4a48;
            border: 0px;}"""
        )
        self.date_created_iLabel.setObjectName("date_created_iLabel")
        self.gridLayout_20.addWidget(self.date_created_iLabel, 2, 0, 1, 1)
        self.auctionID_iLabel = QtWidgets.QLabel(self.iLabels)
        self.auctionID_iLabel.setStyleSheet(
            """QLabel {background-color: #e9f5db; color: #4a4a48;
            border: 0px;}"""
        )
        self.auctionID_iLabel.setObjectName("auctionID_iLabel")
        self.gridLayout_20.addWidget(self.auctionID_iLabel, 0, 0, 1, 1)
        self.end_date_iLabel = QtWidgets.QLabel(self.iLabels)
        self.end_date_iLabel.setStyleSheet(
            """QLabel {background-color: #e9f5db; color: #4a4a48;
            border: 0px;}"""
        )
        self.end_date_iLabel.setObjectName("end_date_iLabel")
        self.gridLayout_20.addWidget(self.end_date_iLabel, 3, 0, 1, 1)
        self.gridLayout_18.addWidget(self.iLabels, 0, 1, 6, 1)
        self.set_texts()

    def set_texts(self):
        self.address_label_2.setText('Address:')
        self.auctionID_label.setText('Auction ID:')
        self.date_created_label.setText('Date created:')
        self.end_date_label_2.setText('End date:')
        self.kvm_label_2.setText('Square meters:')
        self.price_label_2.setText('Current price:')


class finishedAuction(QtWidgets.QFrame):
    def __init__(self, *args, **kwargs):
        QtWidgets.QFrame.__init__(self, *args, **kwargs)
        self.setMinimumSize(QtCore.QSize(720, 250))
        self.setMaximumSize(QtCore.QSize(16777215, 250))
        self.setStyleSheet(
            """QFrame {background-color: #cfe1b9; border-width: 1px;
            border-style: solid; border-color: #718355; border-radius: 5px;}"""
        )
        self.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.setFrameShadow(QtWidgets.QFrame.Raised)
        self.setObjectName("TEMP")
        self.gridLayout_3 = QtWidgets.QGridLayout(self)
        self.gridLayout_3.setContentsMargins(12, 12, 12, 12)
        self.gridLayout_3.setHorizontalSpacing(20)
        self.gridLayout_3.setVerticalSpacing(0)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.graph_container = QtWidgets.QWidget(self)
        self.graph_container.setMinimumSize(QtCore.QSize(225, 226))
        self.graph_container.setMaximumSize(QtCore.QSize(16777215, 226))
        self.graph_container.setStyleSheet("background-color: #cfe1b9;")
        self.graph_container.setObjectName("graph_container")
        self.gridLayout_3.addWidget(self.graph_container, 2, 4, 1, 1)
        self.labels_widget = QtWidgets.QWidget(self)
        self.labels_widget.setMinimumSize(QtCore.QSize(119, 0))
        self.labels_widget.setStyleSheet("background-color: #cfe1b9;")
        self.labels_widget.setObjectName("labels_widget")
        self.verticalLayout_7 = QtWidgets.QVBoxLayout(self.labels_widget)
        self.verticalLayout_7.setObjectName("verticalLayout_7")
        self.owner = QtWidgets.QLabel(self.labels_widget)
        self.owner.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.owner.setStyleSheet(
            "QLabel {color: #4a4a48; border: 0px;}"
        )
        self.owner.setObjectName("owner")
        self.verticalLayout_7.addWidget(self.owner)
        self.winner = QtWidgets.QLabel(self.labels_widget)
        self.winner.setStyleSheet(
            "QLabel {color: #4a4a48; border: 0px;}"
        )
        self.winner.setObjectName("winner")
        self.verticalLayout_7.addWidget(self.winner)
        self.winning = QtWidgets.QLabel(self.labels_widget)
        self.winning.setStyleSheet(
            "QLabel {color: #4a4a48; border: 0px;}"
        )
        self.winning.setObjectName("winning")
        self.verticalLayout_7.addWidget(self.winning)
        self.finished_auction_id = QtWidgets.QLabel(self.labels_widget)
        self.finished_auction_id.setStyleSheet(
            "QLabel {color: #4a4a48; border: 0px;}"
        )
        self.finished_auction_id.setObjectName("finished_auction_id")
        self.verticalLayout_7.addWidget(self.finished_auction_id)
        self.total_winnings = QtWidgets.QLabel(self.labels_widget)
        self.total_winnings.setStyleSheet(
            "QLabel {color: #4a4a48; border: 0px;}"
        )
        self.total_winnings.setObjectName("total_winnings")
        self.verticalLayout_7.addWidget(self.total_winnings)
        self.gridLayout_3.addWidget(self.labels_widget, 2, 1, 1, 1)
        self.iLabels = QtWidgets.QWidget(self)
        sizePolicy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Preferred
        )
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            self.iLabels.sizePolicy().hasHeightForWidth()
        )
        self.iLabels.setSizePolicy(sizePolicy)
        self.iLabels.setMinimumSize(QtCore.QSize(290, 226))
        self.iLabels.setMaximumSize(QtCore.QSize(16777215, 226))
        self.iLabels.setStyleSheet("border-radius: 5px;")
        self.iLabels.setObjectName("iLabels")
        self.verticalLayout_8 = QtWidgets.QVBoxLayout(self.iLabels)
        self.verticalLayout_8.setObjectName("verticalLayout_8")
        self.owner_iLabel = QtWidgets.QLabel(self.iLabels)
        self.owner_iLabel.setStyleSheet(
            "QLabel {color: #4a4a48; border: 0px; background-color: #e9f5db;}"
        )
        self.owner_iLabel.setText("")
        self.owner_iLabel.setObjectName("owner_iLabel")
        self.verticalLayout_8.addWidget(self.owner_iLabel)
        self.winner_iLabel = QtWidgets.QLabel(self.iLabels)
        self.winner_iLabel.setStyleSheet(
            "QLabel {color: #4a4a48; border: 0px; background-color: #e9f5db;}"
        )
        self.winner_iLabel.setText("")
        self.winner_iLabel.setObjectName("winner_iLabel")
        self.verticalLayout_8.addWidget(self.winner_iLabel)
        self.winning_iLabel = QtWidgets.QLabel(self.iLabels)
        self.winning_iLabel.setStyleSheet(
            "QLabel {color: #4a4a48; border: 0px; background-color: #e9f5db;}"
        )
        self.winning_iLabel.setText("")
        self.winning_iLabel.setObjectName("winning_iLabel")
        self.verticalLayout_8.addWidget(self.winning_iLabel)
        self.finished_auction_iLabel = QtWidgets.QLabel(self.iLabels)
        self.finished_auction_iLabel.setStyleSheet(
            "QLabel {color: #4a4a48; border: 0px; background-color: #e9f5db;}"
        )
        self.finished_auction_iLabel.setText("")
        self.finished_auction_iLabel.setObjectName("finished_auction_iLabel")
        self.verticalLayout_8.addWidget(self.finished_auction_iLabel)
        self.total_winnings_iLabel = QtWidgets.QLabel(self.iLabels)
        self.total_winnings_iLabel.setStyleSheet(
            "QLabel {color: #4a4a48; border: 0px; background-color: #e9f5db;}"
        )
        self.total_winnings_iLabel.setText("")
        self.total_winnings_iLabel.setObjectName("total_winnings_iLabel")
        self.verticalLayout_8.addWidget(self.total_winnings_iLabel)
        self.gridLayout_3.addWidget(self.iLabels, 2, 2, 4, 1)

        self.set_texts()

    def set_texts(self):
        self.owner.setText('Owner:')
        self.winner.setText('Winner:')
        self.winning.setText('Winning bid:')
        self.finished_auction_id.setText('Auction ID:')
        self.total_winnings.setText('Total winnings:')


class widgetWithButton(QtWidgets.QFrame):
    def __init__(self, *args, **kwargs):
        QtWidgets.QFrame.__init__(self, *args, **kwargs)
        self.setMinimumSize(QtCore.QSize(720, 250))
        self.setMaximumSize(QtCore.QSize(16777215, 250))
        self.setStyleSheet(
            """QFrame {background-color: #cfe1b9; border-width: 1px;
            border-style: solid; border-color: #718355; border-radius: 5px;}"""
        )
        self.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.setFrameShadow(QtWidgets.QFrame.Raised)
        self.setObjectName("iWidget")
        self.gridLayout_3 = QtWidgets.QGridLayout(self)
        self.gridLayout_3.setHorizontalSpacing(20)
        self.gridLayout_3.setVerticalSpacing(10)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.iLabels = QtWidgets.QWidget(self)
        self.iLabels.setMinimumSize(QtCore.QSize(500, 226))
        self.iLabels.setMaximumSize(QtCore.QSize(16777215, 226))
        self.iLabels.setStyleSheet("border-radius: 5px;")
        self.iLabels.setObjectName("iLabels")
        self.verticalLayout_7 = QtWidgets.QVBoxLayout(self.iLabels)
        self.verticalLayout_7.setObjectName("verticalLayout_7")
        self.auction_iLabel = QtWidgets.QLabel(self.iLabels)
        self.auction_iLabel.setStyleSheet(
            """QLabel {background-color: #e9f5db;
            color: #4a4a48; border: 0px;}"""
        )
        self.auction_iLabel.setText("")
        self.auction_iLabel.setObjectName("auction_iLabel")
        self.verticalLayout_7.addWidget(self.auction_iLabel)
        self.owner_iLabel = QtWidgets.QLabel(self.iLabels)
        self.owner_iLabel.setStyleSheet(
            """QLabel {background-color: #e9f5db;
            color: #4a4a48; border: 0px;}"""
        )
        self.owner_iLabel.setText("")
        self.owner_iLabel.setObjectName("owner_iLabel")
        self.verticalLayout_7.addWidget(self.owner_iLabel)
        self.address_iLabel = QtWidgets.QLabel(self.iLabels)
        self.address_iLabel.setStyleSheet(
            """QLabel {background-color: #e9f5db;
            color: #4a4a48; border: 0px;}"""
        )
        self.address_iLabel.setText("")
        self.address_iLabel.setObjectName("address_iLabel")
        self.verticalLayout_7.addWidget(self.address_iLabel)
        self.date_created_iLabel = QtWidgets.QLabel(self.iLabels)
        self.date_created_iLabel.setStyleSheet(
            """QLabel {background-color: #e9f5db;
            color: #4a4a48; border: 0px;}"""
        )
        self.date_created_iLabel.setText("")
        self.date_created_iLabel.setObjectName("date_created_iLabel")
        self.verticalLayout_7.addWidget(self.date_created_iLabel)
        self.end_date_iLabel = QtWidgets.QLabel(self.iLabels)
        self.end_date_iLabel.setStyleSheet(
            """QLabel {background-color: #e9f5db;
            color: #4a4a48; border: 0px;}"""
        )
        self.end_date_iLabel.setText("")
        self.end_date_iLabel.setObjectName("end_date_iLabel")
        self.verticalLayout_7.addWidget(self.end_date_iLabel)
        self.price_iLabel = QtWidgets.QLabel(self.iLabels)
        self.price_iLabel.setStyleSheet(
            """QLabel {background-color: #e9f5db;
            color: #4a4a48; border: 0px;}"""
        )
        self.price_iLabel.setText("")
        self.price_iLabel.setObjectName("price_iLabel")
        self.verticalLayout_7.addWidget(self.price_iLabel)
        self.kvm_iLabel = QtWidgets.QLabel(self.iLabels)
        self.kvm_iLabel.setStyleSheet(
            """QLabel {background-color: #e9f5db;
            color: #4a4a48; border: 0px;}"""
        )
        self.kvm_iLabel.setText("")
        self.kvm_iLabel.setObjectName("kvm_iLabel")
        self.verticalLayout_7.addWidget(self.kvm_iLabel)
        self.gridLayout_3.addWidget(self.iLabels, 5, 1, 1, 1)
        self.bid_btn = QtWidgets.QPushButton(self)
        self.bid_btn.setMinimumSize(QtCore.QSize(60, 226))
        self.bid_btn.setMaximumSize(QtCore.QSize(60, 226))
        self.bid_btn.setStyleSheet(
            """QPushButton {background-color: #cfe1b9; padding: 4px;
            color:#4a4a48; border-width: 1px; border-style: solid;
            border-radius: 5px; border-color:#718355}
            QPushButton::hover {background-color: #cfe1b9; padding: 4px;
            color: #8e8e8c; border-width: 1px; border-style: solid;
            border-radius: 5px;}
            QPushButton:pressed {background-color: #cfe1b9; padding: 4px;
            color: #e9f5db; border-width: 1px; border-style: solid;
            border-radius: 5px;}"""
        )
        self.bid_btn.setObjectName("bid_btn")
        self.bid_btn.setText("Bid")
        self.bid_btn.clicked.connect(self.clicked)
        self.gridLayout_3.addWidget(self.bid_btn, 5, 2, 1, 1)
        self.widget_2 = QtWidgets.QWidget(self)
        self.widget_2.setStyleSheet("background-color: #cfe1b9;")
        self.widget_2.setObjectName("widget_2")
        self.verticalLayout_8 = QtWidgets.QVBoxLayout(self.widget_2)
        self.verticalLayout_8.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_8.setSpacing(0)
        self.verticalLayout_8.setObjectName("verticalLayout_8")
        self.label_2 = QtWidgets.QLabel(self.widget_2)
        self.label_2.setMinimumSize(QtCore.QSize(0, 20))
        self.label_2.setMaximumSize(QtCore.QSize(16777215, 20))
        self.label_2.setStyleSheet(
            """QLabel {color: #4a4a48; border: 0px;}"""
        )
        self.label_2.setObjectName("label_2")
        self.verticalLayout_8.addWidget(self.label_2)
        self.label_4 = QtWidgets.QLabel(self.widget_2)
        self.label_4.setMinimumSize(QtCore.QSize(0, 20))
        self.label_4.setMaximumSize(QtCore.QSize(16777215, 20))
        self.label_4.setStyleSheet(
            """QLabel {color: #4a4a48; border: 0px;}"""
        )
        self.label_4.setObjectName("label_4")
        self.verticalLayout_8.addWidget(self.label_4)
        self.label_3 = QtWidgets.QLabel(self.widget_2)
        self.label_3.setMinimumSize(QtCore.QSize(0, 20))
        self.label_3.setMaximumSize(QtCore.QSize(16777215, 20))
        self.label_3.setStyleSheet(
            """QLabel {color: #4a4a48; border: 0px;}"""
        )
        self.label_3.setObjectName("label_3")
        self.verticalLayout_8.addWidget(self.label_3)
        self.label_7 = QtWidgets.QLabel(self.widget_2)
        self.label_7.setMinimumSize(QtCore.QSize(0, 20))
        self.label_7.setMaximumSize(QtCore.QSize(16777215, 20))
        self.label_7.setStyleSheet(
            """QLabel {color: #4a4a48; border: 0px;}"""
        )
        self.label_7.setObjectName("label_7")
        self.verticalLayout_8.addWidget(self.label_7)
        self.label_6 = QtWidgets.QLabel(self.widget_2)
        self.label_6.setMinimumSize(QtCore.QSize(0, 20))
        self.label_6.setMaximumSize(QtCore.QSize(16777215, 20))
        self.label_6.setStyleSheet(
            """QLabel {color: #4a4a48; border: 0px;}"""
        )
        self.label_6.setObjectName("label_6")
        self.verticalLayout_8.addWidget(self.label_6)
        self.label_5 = QtWidgets.QLabel(self.widget_2)
        self.label_5.setMinimumSize(QtCore.QSize(0, 20))
        self.label_5.setMaximumSize(QtCore.QSize(16777215, 20))
        self.label_5.setStyleSheet(
            """QLabel {color: #4a4a48; border: 0px;}"""
        )
        self.label_5.setObjectName("label_5")
        self.verticalLayout_8.addWidget(self.label_5)
        self.label = QtWidgets.QLabel(self.widget_2)
        self.label.setMinimumSize(QtCore.QSize(0, 20))
        self.label.setMaximumSize(QtCore.QSize(16777215, 20))
        self.label.setStyleSheet(
            """QLabel {color: #4a4a48; border: 0px;}"""
        )
        self.label.setObjectName("label")
        self.verticalLayout_8.addWidget(self.label)
        self.gridLayout_3.addWidget(self.widget_2, 5, 0, 1, 1)
        self.selected = False
        self.set_texts()

    def clicked(self):
        self.selected = True

    def set_texts(self):
        self.label_2.setText('Auction ID:')
        self.label_4.setText('Owner:')
        self.label_3.setText('Address:')
        self.label_7.setText('Date created:')
        self.label_6.setText('End date:')
        self.label_5.setText('Current price:')
        self.label.setText('Square meters:')


class bidWidget(QtWidgets.QFrame):
    def __init__(self, *args, **kwargs):
        QtWidgets.QFrame.__init__(self, *args, **kwargs)
        self.setMinimumSize(QtCore.QSize(720, 250))
        self.setMaximumSize(QtCore.QSize(16777215, 250))
        self.setStyleSheet(
            """QFrame {background-color: #cfe1b9; border-width: 1px;
            border-style: solid; border-color: #718355; border-radius: 5px;}"""
        )
        self.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.setFrameShadow(QtWidgets.QFrame.Raised)
        self.setObjectName("iWidget")
        self.gridLayout_3 = QtWidgets.QGridLayout(self)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.label_widget = QtWidgets.QWidget(self)
        self.label_widget.setStyleSheet("background-color: #cfe1b9;")
        self.label_widget.setObjectName("label_widget")
        self.verticalLayout_7 = QtWidgets.QVBoxLayout(self.label_widget)
        self.verticalLayout_7.setContentsMargins(10, 10, 0, 10)
        self.verticalLayout_7.setObjectName("verticalLayout_7")
        self.owner_label = QtWidgets.QLabel(self.label_widget)
        self.owner_label.setStyleSheet(
            "QLabel {color: #4a4a48; border: 0px;}"
        )
        self.owner_label.setObjectName("owner_label")
        self.verticalLayout_7.addWidget(self.owner_label)
        self.address_label_2 = QtWidgets.QLabel(self.label_widget)
        self.address_label_2.setStyleSheet(
            "QLabel {color: #4a4a48; border: 0px;}"
        )
        self.address_label_2.setObjectName("address_label_2")
        self.verticalLayout_7.addWidget(self.address_label_2)
        self.label = QtWidgets.QLabel(self.label_widget)
        self.label.setStyleSheet(
            "QLabel {color: #4a4a48; border: 0px;}"
        )
        self.label.setObjectName("label")
        self.verticalLayout_7.addWidget(self.label)
        self.created_label = QtWidgets.QLabel(self.label_widget)
        self.created_label.setStyleSheet(
            "QLabel {color: #4a4a48; border: 0px;}"
        )
        self.created_label.setObjectName("created_label")
        self.verticalLayout_7.addWidget(self.created_label)
        self.end_date_label_2 = QtWidgets.QLabel(self.label_widget)
        self.end_date_label_2.setStyleSheet(
            "QLabel {color: #4a4a48; border: 0px;}"
        )
        self.end_date_label_2.setObjectName("end_date_label_2")
        self.verticalLayout_7.addWidget(self.end_date_label_2)
        self.bid_leader = QtWidgets.QLabel(self.label_widget)
        self.bid_leader.setStyleSheet(
            "QLabel {color: #4a4a48; border: 0px;}"
        )
        self.bid_leader.setObjectName("bid_leader")
        self.verticalLayout_7.addWidget(self.bid_leader)
        self.leading_bid = QtWidgets.QLabel(self.label_widget)
        self.leading_bid.setStyleSheet(
            "QLabel {color: #4a4a48; border: 0px;}"
        )
        self.leading_bid.setObjectName("leading_bid")
        self.verticalLayout_7.addWidget(self.leading_bid)
        self.gridLayout_3.addWidget(self.label_widget, 0, 0, 1, 1)
        self.bid_insert_label = QtWidgets.QLabel(self)
        self.bid_insert_label.setMinimumSize(QtCore.QSize(120, 0))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.bid_insert_label.setFont(font)
        self.bid_insert_label.setStyleSheet(
            "QLabel {color: #4a4a48; border: 0px;}"
        )
        self.bid_insert_label.setObjectName("bid_insert_label")
        self.gridLayout_3.addWidget(self.bid_insert_label, 1, 0, 1, 1)
        self.iLabels = QtWidgets.QWidget(self)
        self.iLabels.setStyleSheet("border-radius: 5px;")
        self.iLabels.setObjectName("iLabels")
        self.verticalLayout_9 = QtWidgets.QVBoxLayout(self.iLabels)
        self.verticalLayout_9.setContentsMargins(-1, 10, 0, 10)
        self.verticalLayout_9.setObjectName("verticalLayout_9")
        self.owner_iLabel = QtWidgets.QLabel(self.iLabels)
        self.owner_iLabel.setStyleSheet(
            "QLabel {background-color: #e9f5db; color: #4a4a48; border: 0px;}"
        )
        self.owner_iLabel.setText("")
        self.owner_iLabel.setObjectName("owner_iLabel")
        self.verticalLayout_9.addWidget(self.owner_iLabel)
        self.address_iLabel = QtWidgets.QLabel(self.iLabels)
        self.address_iLabel.setStyleSheet(
            "QLabel {background-color: #e9f5db; color: #4a4a48; border: 0px;}"
        )
        self.address_iLabel.setText("")
        self.address_iLabel.setObjectName("address_iLabel")
        self.verticalLayout_9.addWidget(self.address_iLabel)
        self.kvm_iLabel = QtWidgets.QLabel(self.iLabels)
        self.kvm_iLabel.setStyleSheet(
            "QLabel {background-color: #e9f5db; color: #4a4a48; border: 0px;}"
        )
        self.kvm_iLabel.setText("")
        self.kvm_iLabel.setObjectName("kvm_iLabel")
        self.verticalLayout_9.addWidget(self.kvm_iLabel)
        self.created_iLabel = QtWidgets.QLabel(self.iLabels)
        self.created_iLabel.setStyleSheet(
            "QLabel {background-color: #e9f5db; color: #4a4a48; border: 0px;}"
        )
        self.created_iLabel.setText("")
        self.created_iLabel.setObjectName("created_iLabel")
        self.verticalLayout_9.addWidget(self.created_iLabel)
        self.end_date_iLabel = QtWidgets.QLabel(self.iLabels)
        self.end_date_iLabel.setStyleSheet(
            "QLabel {background-color: #e9f5db; color: #4a4a48; border: 0px;}"
        )
        self.end_date_iLabel.setText("")
        self.end_date_iLabel.setObjectName("end_date_iLabel")
        self.verticalLayout_9.addWidget(self.end_date_iLabel)
        self.bid_leader_iLabel = QtWidgets.QLabel(self.iLabels)
        self.bid_leader_iLabel.setStyleSheet(
            "QLabel {background-color: #e9f5db; color: #4a4a48; border: 0px;}"
        )
        self.bid_leader_iLabel.setText("")
        self.bid_leader_iLabel.setObjectName("bid_leader_iLabel")
        self.verticalLayout_9.addWidget(self.bid_leader_iLabel)
        self.leading_bid_iLabel = QtWidgets.QLabel(self.iLabels)
        self.leading_bid_iLabel.setStyleSheet(
            "QLabel {background-color: #e9f5db; color: #4a4a48; border: 0px;}"
        )
        self.leading_bid_iLabel.setText("")
        self.leading_bid_iLabel.setObjectName("leading_bid_iLabel")
        self.verticalLayout_9.addWidget(self.leading_bid_iLabel)
        self.gridLayout_3.addWidget(self.iLabels, 0, 1, 1, 1)
        self.bid_insert_lineEdit = QtWidgets.QLineEdit(self)
        self.bid_insert_lineEdit.setStyleSheet(
            """QLineEdit {padding: 4px; background-color: #e9f5db;
            border-width: 1px; border-style: solid; border-radius: 5px;
            border-color: #718355; selection-background-color: #cfe1b9;}"""
        )
        self.bid_insert_lineEdit.setObjectName("bid_insert_lineEdit")
        self.gridLayout_3.addWidget(self.bid_insert_lineEdit, 1, 1, 1, 1)
        self.submit_bid = QtWidgets.QPushButton(self)
        self.submit_bid.setMinimumSize(QtCore.QSize(60, 226))
        self.submit_bid.setMaximumSize(QtCore.QSize(60, 226))
        self.submit_bid.setStyleSheet(
            """QPushButton {padding: 4px; color:#4a4a48; border-width: 1px;
            border-style: solid; border-radius: 5px; border-color:#718355}
            QPushButton::hover {padding: 4px; color: #8e8e8c;
            border-width: 1px; border-style: solid; border-radius: 5px;}
            QPushButton:pressed {padding: 4px; color: #e9f5db;
            border-width: 1px; border-style: solid; border-radius: 5px;}"""
        )
        self.submit_bid.setObjectName("submit_bid")
        self.gridLayout_3.addWidget(self.submit_bid, 0, 2, 1, 1)
        self.set_texts()

    def set_texts(self):
        self.owner_label.setText('Owner:')
        self.address_label_2.setText('Address:')
        self.label.setText('Square meters:')
        self.created_label.setText('End date:')
        self.end_date_label_2.setText('Current price:')
        self.bid_leader.setText('Bid leader:')
        self.leading_bid.setText('Leading bid:')
        self.bid_insert_label.setText('   Your bid:')
        self.submit_bid.setText('Submit')


if __name__ == '__main__':
    pass
