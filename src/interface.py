from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        self.setObjectName("MainWindow")
        self.resize(1200, 700)
        self.setMinimumSize(QtCore.QSize(1000, 600))
        self.centralwidget = QtWidgets.QWidget(self)
        self.centralwidget.setObjectName("centralwidget")
        self.centralGridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.centralGridLayout.setContentsMargins(0, 0, 0, 0)
        self.centralGridLayout.setSpacing(0)
        self.centralGridLayout.setObjectName("centralGridLayout")
        self.top_frame = QtWidgets.QFrame(self.centralwidget)
        self.top_frame.setMinimumSize(QtCore.QSize(0, 100))
        self.top_frame.setMaximumSize(QtCore.QSize(16777215, 100))
        self.top_frame.setStyleSheet(
            """QFrame {border-bottom: 1px; border-color: #6b9080;
            border-style:solid; background-color: #cfe1b9}"""
        )
        self.top_frame.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.top_frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.top_frame.setObjectName("top_frame")
        self.topGridLayout = QtWidgets.QGridLayout(self.top_frame)
        self.topGridLayout.setContentsMargins(0, 0, 0, 0)
        self.topGridLayout.setSpacing(0)
        self.topGridLayout.setObjectName("topGridLayout")
        self.stacked_top = QtWidgets.QStackedWidget(self.top_frame)
        self.stacked_top.setMinimumSize(QtCore.QSize(50, 0))
        self.stacked_top.setStyleSheet(
            "QStackedWidget {border: 0px; background-color: #e9f5db;}"
        )
        self.stacked_top.setObjectName("stacked_top")
        self.top_page1 = QtWidgets.QWidget()
        self.top_page1.setStyleSheet("background-color: #cfe1b9;")
        self.top_page1.setObjectName("top_page1")
        self.topPage1GridLayout = QtWidgets.QGridLayout(self.top_page1)
        self.topPage1GridLayout.setContentsMargins(0, 0, 0, 0)
        self.topPage1GridLayout.setSpacing(0)
        self.topPage1GridLayout.setObjectName("topPage1GridLayout")
        self.login_register_widget = QtWidgets.QWidget(self.top_page1)
        self.login_register_widget.setMinimumSize(QtCore.QSize(120, 0))
        self.login_register_widget.setMaximumSize(QtCore.QSize(120, 16777215))
        self.login_register_widget.setStyleSheet("")
        self.login_register_widget.setObjectName("login_register_widget")
        self.loginVLayout = QtWidgets.QVBoxLayout(self.login_register_widget)
        self.loginVLayout.setContentsMargins(15, 0, 0, 0)
        self.loginVLayout.setSpacing(0)
        self.loginVLayout.setObjectName("loginVLayout")
        self.login = QtWidgets.QPushButton(self.login_register_widget)
        sizePolicy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed
        )
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            self.login.sizePolicy().hasHeightForWidth()
        )
        self.login.setSizePolicy(sizePolicy)
        self.login.setMinimumSize(QtCore.QSize(80, 26))
        self.login.setMaximumSize(QtCore.QSize(80, 26))
        self.login.setStyleSheet(
            """QPushButton {padding: 4px; color:#4a4a48; border-width: 1px;
            border-style: solid; border-radius: 5px; border-color:#718355}
            QPushButton::hover {padding: 4px; color: #8e8e8c;
            border-width: 1px; border-style: solid; border-radius: 5px;}
            QPushButton:pressed {padding: 4px; color: #e9f5db;
            border-width: 1px; border-style: solid; border-radius: 5px;}"""
        )
        self.login.setObjectName("login")
        self.loginVLayout.addWidget(self.login)
        self.register_2 = QtWidgets.QPushButton(self.login_register_widget)
        self.register_2.setMinimumSize(QtCore.QSize(80, 26))
        self.register_2.setMaximumSize(QtCore.QSize(80, 26))
        self.register_2.setStyleSheet(
            """QPushButton {padding: 4px; color:#4a4a48; border-width: 1px;
            border-style: solid; border-radius: 5px; border-color:#718355}
            QPushButton::hover {padding: 4px; color: #8e8e8c;
            border-width: 1px; border-style: solid; border-radius: 5px;}
            QPushButton:pressed {padding: 4px; color: #e9f5db;
            border-width: 1px; border-style: solid; border-radius: 5px;}"""
        )
        self.register_2.setObjectName("register_2")
        self.loginVLayout.addWidget(self.register_2)
        self.topPage1GridLayout.addWidget(
            self.login_register_widget, 0, 1, 1, 1
        )
        self.filler_widget = QtWidgets.QWidget(self.top_page1)
        self.filler_widget.setObjectName("filler_widget")
        self.topPage1GridLayout.addWidget(self.filler_widget, 0, 0, 1, 1)
        self.stacked_top.addWidget(self.top_page1)
        self.top_page2 = QtWidgets.QWidget()
        self.top_page2.setStyleSheet("background-color: #cfe1b9;")
        self.top_page2.setObjectName("top_page2")
        self.topPage2GridLayout = QtWidgets.QGridLayout(self.top_page2)
        self.topPage2GridLayout.setContentsMargins(0, 0, 0, 0)
        self.topPage2GridLayout.setSpacing(0)
        self.topPage2GridLayout.setObjectName("topPage2GridLayout")
        self.stacked_top.addWidget(self.top_page2)
        self.topGridLayout.addWidget(self.stacked_top, 1, 1, 1, 1)
        self.logo = QtWidgets.QWidget(self.top_frame)
        self.logo.setMinimumSize(QtCore.QSize(250, 0))
        self.logo.setMaximumSize(QtCore.QSize(300, 16777215))
        self.logo.setStyleSheet("QWidget {border:0px;}")
        self.logo.setObjectName("logo")
        self.logoLayout = QtWidgets.QGridLayout(self.logo)
        self.logoLayout.setObjectName("logoLayout")
        self.logo_label = QtWidgets.QLabel(self.logo)
        font = QtGui.QFont()
        font.setFamily("Courier New")
        font.setPointSize(26)
        font.setBold(True)
        font.setItalic(False)
        self.logo_label.setFont(font)
        self.logo_label.setObjectName("logo_label")
        self.logoLayout.addWidget(self.logo_label, 0, 0, 1, 1)
        self.topGridLayout.addWidget(self.logo, 1, 0, 1, 1)
        self.centralGridLayout.addWidget(self.top_frame, 0, 0, 1, 1)
        self.main_body_frame = QtWidgets.QFrame(self.centralwidget)
        self.main_body_frame.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.main_body_frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.main_body_frame.setObjectName("main_body_frame")
        self.mainBodyLayout = QtWidgets.QHBoxLayout(self.main_body_frame)
        self.mainBodyLayout.setContentsMargins(0, 0, 0, 0)
        self.mainBodyLayout.setSpacing(0)
        self.mainBodyLayout.setObjectName("mainBodyLayout")
        self.left_widget = QtWidgets.QWidget(self.main_body_frame)
        sizePolicy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred
        )
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            self.left_widget.sizePolicy().hasHeightForWidth()
        )
        self.left_widget.setSizePolicy(sizePolicy)
        self.left_widget.setMinimumSize(QtCore.QSize(250, 0))
        self.left_widget.setMaximumSize(QtCore.QSize(250, 16777215))
        self.left_widget.setStyleSheet(
            """QWidget {background-color: #dff2ca; border-right: 1px;
            border-style:solid; border-color:  #c7d3b9;}"""
        )
        self.left_widget.setObjectName("left_widget")
        self.leftGridLayout = QtWidgets.QGridLayout(self.left_widget)
        self.leftGridLayout.setContentsMargins(0, 0, 0, 0)
        self.leftGridLayout.setSpacing(0)
        self.leftGridLayout.setObjectName("leftGridLayout")
        self.stackedWidget = QtWidgets.QStackedWidget(self.left_widget)
        self.stackedWidget.setStyleSheet("")
        self.stackedWidget.setObjectName("stackedWidget")
        self.logged_in = QtWidgets.QWidget()
        self.logged_in.setStyleSheet("QWidget {border:0px;}")
        self.logged_in.setObjectName("logged_in")
        self.verticalLayout_4 = QtWidgets.QVBoxLayout(self.logged_in)
        self.verticalLayout_4.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_4.setSpacing(0)
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.current_user = QtWidgets.QLabel(self.logged_in)
        self.current_user.setMinimumSize(QtCore.QSize(230, 30))
        self.current_user.setMaximumSize(QtCore.QSize(230, 30))
        self.current_user.setStyleSheet(
            "QLabel {color: #4a4a48; border: 1px;}"
        )
        self.current_user.setText("")
        self.current_user.setAlignment(QtCore.Qt.AlignCenter)
        self.current_user.setObjectName("current_user")
        self.verticalLayout_4.addWidget(
            self.current_user, 0, QtCore.Qt.AlignHCenter
        )
        self.balance_label = QtWidgets.QLabel(self.logged_in)
        self.balance_label.setMinimumSize(QtCore.QSize(230, 30))
        self.balance_label.setMaximumSize(QtCore.QSize(230, 30))
        self.balance_label.setText("")
        self.balance_label.setAlignment(QtCore.Qt.AlignCenter)
        self.balance_label.setObjectName("balance_label")
        self.verticalLayout_4.addWidget(
            self.balance_label, 0, QtCore.Qt.AlignHCenter
        )
        self.create_auction_widget = QtWidgets.QWidget(self.logged_in)
        self.create_auction_widget.setMinimumSize(QtCore.QSize(250, 100))
        self.create_auction_widget.setMaximumSize(QtCore.QSize(250, 100))
        self.create_auction_widget.setStyleSheet("QWidget {border:0px;}")
        self.create_auction_widget.setObjectName("create_auction_widget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.create_auction_widget)
        self.verticalLayout.setContentsMargins(26, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.create_auction_btn = QtWidgets.QPushButton(
            self.create_auction_widget
        )
        self.create_auction_btn.setMinimumSize(QtCore.QSize(110, 26))
        self.create_auction_btn.setMaximumSize(QtCore.QSize(110, 26))
        self.create_auction_btn.setStyleSheet(
            """QPushButton {padding: 4px; color:#4a4a48; border-width: 1px;
            border-style: solid; border-radius: 5px; border-color:#718355}
            QPushButton::hover {padding: 4px; color: #8e8e8c;
            border-width: 1px; border-style: solid; border-radius: 5px;}
            QPushButton:pressed {padding: 4px; color: #e9f5db;
            border-width: 1px; border-style: solid; border-radius: 5px;}"""
        )
        self.create_auction_btn.setObjectName("create_auction_btn")
        self.verticalLayout.addWidget(self.create_auction_btn)
        self.verticalLayout_4.addWidget(self.create_auction_widget, 0, QtCore.Qt.AlignHCenter)
        self.my_auctions = QtWidgets.QWidget(self.logged_in)
        self.my_auctions.setMinimumSize(QtCore.QSize(250, 100))
        self.my_auctions.setMaximumSize(QtCore.QSize(250, 100))
        self.my_auctions.setStyleSheet("QWidget {border:0px;}")
        self.my_auctions.setObjectName("my_auctions")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout(self.my_auctions)
        self.verticalLayout_3.setContentsMargins(26, 0, 0, 0)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.view_auctions_btn = QtWidgets.QPushButton(self.my_auctions)
        self.view_auctions_btn.setMinimumSize(QtCore.QSize(120, 26))
        self.view_auctions_btn.setMaximumSize(QtCore.QSize(100, 26))
        self.view_auctions_btn.setStyleSheet(
            """QPushButton {padding: 4px; color:#4a4a48; border-width: 1px;
            border-style: solid; border-radius: 5px; border-color:#718355}
            QPushButton::hover {padding: 4px; color: #8e8e8c;
            border-width: 1px; border-style: solid; border-radius: 5px;}
            QPushButton:pressed {padding: 4px; color: #e9f5db;
            border-width: 1px; border-style: solid; border-radius: 5px;}"""
        )
        self.view_auctions_btn.setObjectName("view_auctions_btn")
        self.verticalLayout_3.addWidget(self.view_auctions_btn)
        self.pushButton = QtWidgets.QPushButton(self.my_auctions)
        self.pushButton.setMinimumSize(QtCore.QSize(120, 26))
        self.pushButton.setMaximumSize(QtCore.QSize(120, 26))
        self.pushButton.setStyleSheet(
            """QPushButton {padding: 4px; color:#4a4a48; border-width: 1px;
            border-style: solid; border-radius: 5px; border-color:#718355}
            QPushButton::hover {padding: 4px; color: #8e8e8c;
            border-width: 1px; border-style: solid; border-radius: 5px;}
            QPushButton:pressed {padding: 4px; color: #e9f5db;
            border-width: 1px; border-style: solid; border-radius: 5px;}"""
        )
        self.pushButton.setObjectName("pushButton")
        self.verticalLayout_3.addWidget(self.pushButton)
        self.verticalLayout_4.addWidget(
            self.my_auctions, 0, QtCore.Qt.AlignHCenter
        )
        self.widget_2 = QtWidgets.QWidget(self.logged_in)
        self.widget_2.setMinimumSize(QtCore.QSize(0, 150))
        self.widget_2.setObjectName("widget_2")
        self.verticalLayout_4.addWidget(self.widget_2)
        self.widget = QtWidgets.QWidget(self.logged_in)
        self.widget.setMinimumSize(QtCore.QSize(250, 46))
        self.widget.setMaximumSize(QtCore.QSize(250, 46))
        self.widget.setObjectName("widget")
        self.gridLayout_3 = QtWidgets.QGridLayout(self.widget)
        self.gridLayout_3.setContentsMargins(26, 0, 26, 0)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.home_btn = QtWidgets.QPushButton(self.widget)
        self.home_btn.setStyleSheet(
            """QPushButton {padding: 4px; color:#4a4a48; border-width: 1px;
            border-style: solid; border-radius: 5px; border-color:#718355}
            QPushButton::hover {padding: 4px; color: #8e8e8c;
            border-width: 1px; border-style: solid; border-radius: 5px;}
            QPushButton:pressed {padding: 4px; color: #e9f5db;
            border-width: 1px; border-style: solid; border-radius: 5px;}"""
        )
        self.home_btn.setObjectName("home_btn")
        self.gridLayout_3.addWidget(self.home_btn, 0, 0, 1, 1)
        self.verticalLayout_4.addWidget(self.widget, 0, QtCore.Qt.AlignBottom)
        self.home_settings_widget = QtWidgets.QWidget(self.logged_in)
        self.home_settings_widget.setMinimumSize(QtCore.QSize(250, 46))
        self.home_settings_widget.setMaximumSize(QtCore.QSize(250, 46))
        self.home_settings_widget.setObjectName("home_settings_widget")
        self.homeGridLayout = QtWidgets.QGridLayout(self.home_settings_widget)
        self.homeGridLayout.setContentsMargins(0, 0, 0, 0)
        self.homeGridLayout.setSpacing(0)
        self.homeGridLayout.setObjectName("homeGridLayout")
        self.logout_btn = QtWidgets.QPushButton(self.home_settings_widget)
        self.logout_btn.setMinimumSize(QtCore.QSize(80, 26))
        self.logout_btn.setMaximumSize(QtCore.QSize(80, 26))
        self.logout_btn.setStyleSheet(
            """QPushButton {padding: 4px; color:#4a4a48; border-width: 1px;
            border-style: solid; border-radius: 5px; border-color:#718355}
            QPushButton::hover {padding: 4px; color: #8e8e8c;
            border-width: 1px; border-style: solid; border-radius: 5px;}
            QPushButton:pressed {padding: 4px; color: #e9f5db;
            border-width: 1px; border-style: solid; border-radius: 5px;}"""
        )
        self.logout_btn.setObjectName("logout_btn")
        self.homeGridLayout.addWidget(self.logout_btn, 0, 0, 1, 1)
        self.my_auctions_btn = QtWidgets.QPushButton(self.home_settings_widget)
        self.my_auctions_btn.setMinimumSize(QtCore.QSize(90, 26))
        self.my_auctions_btn.setMaximumSize(QtCore.QSize(90, 26))
        self.my_auctions_btn.setStyleSheet(
            """QPushButton {padding: 4px; color:#4a4a48; border-width: 1px;
            border-style: solid; border-radius: 5px; border-color:#718355}
            QPushButton::hover {padding: 4px; color: #8e8e8c;
            border-width: 1px; border-style: solid; border-radius: 5px;}
            QPushButton:pressed {padding: 4px; color: #e9f5db;
            border-width: 1px; border-style: solid; border-radius: 5px;}"""
        )
        self.my_auctions_btn.setObjectName("my_auctions_btn")
        self.homeGridLayout.addWidget(self.my_auctions_btn, 0, 1, 1, 1)
        self.verticalLayout_4.addWidget(
            self.home_settings_widget, 0, QtCore.Qt.AlignBottom
        )
        self.stackedWidget.addWidget(self.logged_in)
        self.home = QtWidgets.QWidget()
        self.home.setStyleSheet("QWidget {border:0px;}")
        self.home.setObjectName("home")
        self.stackedWidget.addWidget(self.home)
        self.leftGridLayout.addWidget(self.stackedWidget, 5, 0, 1, 1)
        self.mainBodyLayout.addWidget(self.left_widget)
        self.body_frame = QtWidgets.QFrame(self.main_body_frame)
        self.body_frame.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.body_frame.setFrameShadow(QtWidgets.QFrame.Plain)
        self.body_frame.setLineWidth(0)
        self.body_frame.setObjectName("body_frame")
        self.bodyGridLayout = QtWidgets.QGridLayout(self.body_frame)
        self.bodyGridLayout.setContentsMargins(0, 0, 0, 0)
        self.bodyGridLayout.setSpacing(0)
        self.bodyGridLayout.setObjectName("bodyGridLayout")
        self.main_body = QtWidgets.QStackedWidget(self.body_frame)
        self.main_body.setStyleSheet(
            "background-color: #e9f5db; border:0px;"
        )
        self.main_body.setLineWidth(0)
        self.main_body.setObjectName("main_body")
        self.general_page = QtWidgets.QWidget()
        self.general_page.setStyleSheet("background-color: #e9f5db;")
        self.general_page.setObjectName("general_page")
        self.gridLayout_5 = QtWidgets.QGridLayout(self.general_page)
        self.gridLayout_5.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_5.setSpacing(0)
        self.gridLayout_5.setObjectName("gridLayout_5")
        self.textBrowser = QtWidgets.QTextBrowser(self.general_page)
        self.textBrowser.setStyleSheet(
            "QTextBrowser {background-color: #e9f5db;}"
        )
        self.textBrowser.setVerticalScrollBarPolicy(
            QtCore.Qt.ScrollBarAlwaysOff
        )
        self.textBrowser.setHorizontalScrollBarPolicy(
            QtCore.Qt.ScrollBarAlwaysOff
        )
        self.textBrowser.setObjectName("textBrowser")
        self.gridLayout_5.addWidget(self.textBrowser, 0, 0, 1, 1)
        self.main_body.addWidget(self.general_page)
        self.login_page = QtWidgets.QWidget()
        self.login_page.setStyleSheet("background-color: #e9f5db;")
        self.login_page.setObjectName("login_page")
        self.gridLayout_6 = QtWidgets.QGridLayout(self.login_page)
        self.gridLayout_6.setObjectName("gridLayout_6")
        self.login_frame = QtWidgets.QFrame(self.login_page)
        self.login_frame.setMinimumSize(QtCore.QSize(250, 350))
        self.login_frame.setMaximumSize(QtCore.QSize(250, 350))
        self.login_frame.setStyleSheet(
            """QFrame {background-color: #cfe1b9; border-width: 1px;
            border-style: solid; border-color: #718355; border-radius: 5px;}"""
        )
        self.login_frame.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.login_frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.login_frame.setObjectName("login_frame")
        self.gridLayout_7 = QtWidgets.QGridLayout(self.login_frame)
        self.gridLayout_7.setContentsMargins(20, 50, 20, 20)
        self.gridLayout_7.setHorizontalSpacing(50)
        self.gridLayout_7.setVerticalSpacing(15)
        self.gridLayout_7.setObjectName("gridLayout_7")
        self.password_input = QtWidgets.QLineEdit(self.login_frame)
        self.password_input.setMinimumSize(QtCore.QSize(200, 0))
        self.password_input.setMaximumSize(QtCore.QSize(200, 16777215))
        self.password_input.setStyleSheet(
            """QLineEdit {padding: 4px; background-color: #e9f5db;
            border-width: 1px; border-style: solid; border-radius: 5px;
            border-color:#718355; selection-background-color: #cfe1b9;}"""
        )
        self.password_input.setObjectName("password_input")
        self.gridLayout_7.addWidget(
            self.password_input, 4, 0, 1, 2, QtCore.Qt.AlignHCenter
        )
        self.email_input = QtWidgets.QLineEdit(self.login_frame)
        self.email_input.setMinimumSize(QtCore.QSize(200, 0))
        self.email_input.setMaximumSize(QtCore.QSize(200, 16777215))
        self.email_input.setStyleSheet(
            """QLineEdit {padding: 4px; background-color: #e9f5db;
            border-width: 1px; border-style: solid; border-radius: 5px;
            border-color: #718355; selection-background-color: #cfe1b9;}"""
        )
        self.email_input.setObjectName("email_input")
        self.gridLayout_7.addWidget(
            self.email_input, 1, 0, 1, 2, QtCore.Qt.AlignHCenter
        )
        self.password_label = QtWidgets.QLabel(self.login_frame)
        self.password_label.setMinimumSize(QtCore.QSize(180, 20))
        self.password_label.setMaximumSize(QtCore.QSize(180, 20))
        self.password_label.setStyleSheet(
            "color: #4a4a48; border:0px;"
        )
        self.password_label.setObjectName("password_label")
        self.gridLayout_7.addWidget(
            self.password_label, 3, 0, 1, 2, QtCore.Qt.AlignHCenter
        )
        self.email_label = QtWidgets.QLabel(self.login_frame)
        self.email_label.setMinimumSize(QtCore.QSize(180, 20))
        self.email_label.setMaximumSize(QtCore.QSize(180, 20))
        self.email_label.setStyleSheet("color: #4a4a48; border: 0px;")
        self.email_label.setObjectName("email_label")
        self.gridLayout_7.addWidget(
            self.email_label, 0, 0, 1, 2, QtCore.Qt.AlignHCenter
        )
        self.widget_4 = QtWidgets.QWidget(self.login_frame)
        self.widget_4.setStyleSheet("background-color: #cfe1b9;")
        self.widget_4.setObjectName("widget_4")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.widget_4)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.sign_in = QtWidgets.QPushButton(self.widget_4)
        self.sign_in.setMinimumSize(QtCore.QSize(80, 26))
        self.sign_in.setMaximumSize(QtCore.QSize(26, 16777215))
        self.sign_in.setStyleSheet(
            """QPushButton {padding: 4px; color:#4a4a48; border-width: 1px;
            border-style: solid; border-radius: 5px; border-color:#718355}
            QPushButton::hover {padding: 4px; color: #8e8e8c;
            border-width: 1px; border-style: solid; border-radius: 5px;}
            QPushButton:pressed {padding: 4px; color: #e9f5db;
            border-width: 1px; border-style: solid; border-radius: 5px;}"""
        )
        self.sign_in.setObjectName("sign_in")
        self.horizontalLayout_2.addWidget(self.sign_in)
        self.back_button = QtWidgets.QPushButton(self.widget_4)
        self.back_button.setMinimumSize(QtCore.QSize(80, 26))
        self.back_button.setMaximumSize(QtCore.QSize(80, 26))
        self.back_button.setStyleSheet(
            """QPushButton {padding: 4px; color:#4a4a48; border-width: 1px;
            border-style: solid; border-radius: 5px; border-color:#718355}
            QPushButton::hover {padding: 4px; color: #8e8e8c;
            border-width: 1px; border-style: solid; border-radius: 5px;}
            QPushButton:pressed {padding: 4px; color: #e9f5db;
            border-width: 1px; border-style: solid; border-radius: 5px;}"""
        )
        self.back_button.setObjectName("back_button")
        self.horizontalLayout_2.addWidget(self.back_button)
        self.gridLayout_7.addWidget(
            self.widget_4, 8, 0, 1, 2, QtCore.Qt.AlignHCenter
        )
        self.email_error = QtWidgets.QLabel(self.login_frame)
        self.email_error.setMinimumSize(QtCore.QSize(200, 20))
        self.email_error.setMaximumSize(QtCore.QSize(200, 20))
        self.email_error.setStyleSheet("color: #4a4a48; border:0px;")
        self.email_error.setText("")
        self.email_error.setObjectName("email_error")
        self.gridLayout_7.addWidget(self.email_error, 5, 0, 1, 1)
        self.gridLayout_6.addWidget(self.login_frame, 0, 0, 1, 1)
        self.main_body.addWidget(self.login_page)
        self.register_page = QtWidgets.QWidget()
        self.register_page.setStyleSheet("background-color: #e9f5db;")
        self.register_page.setObjectName("register_page")
        self.gridLayout_9 = QtWidgets.QGridLayout(self.register_page)
        self.gridLayout_9.setObjectName("gridLayout_9")
        self.register_frame = QtWidgets.QFrame(self.register_page)
        self.register_frame.setMinimumSize(QtCore.QSize(250, 420))
        self.register_frame.setMaximumSize(QtCore.QSize(250, 420))
        self.register_frame.setStyleSheet(
            """QFrame {background-color: #cfe1b9; border-width: 1px;
            border-style: solid; border-color: #718355; border-radius: 5px;}"""
        )
        self.register_frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.register_frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.register_frame.setObjectName("register_frame")
        self.gridLayout_10 = QtWidgets.QGridLayout(self.register_frame)
        self.gridLayout_10.setObjectName("gridLayout_10")
        self.widget_5 = QtWidgets.QWidget(self.register_frame)
        self.widget_5.setStyleSheet("background-color: #cfe1b9;")
        self.widget_5.setObjectName("widget_5")
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout(self.widget_5)
        self.horizontalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_3.setSpacing(0)
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.done_button = QtWidgets.QPushButton(self.widget_5)
        self.done_button.setMinimumSize(QtCore.QSize(80, 26))
        self.done_button.setMaximumSize(QtCore.QSize(80, 26))
        self.done_button.setStyleSheet(
            """QPushButton {padding: 4px; color:#4a4a48; border-width: 1px;
            border-style: solid; border-radius: 5px; border-color:#718355}
            QPushButton::hover {padding: 4px; color: #8e8e8c;
            border-width: 1px; border-style: solid; border-radius: 5px;}
            QPushButton:pressed {padding: 4px; color: #e9f5db;
            border-width: 1px; border-style: solid; border-radius: 5px;}"""
        )
        self.done_button.setObjectName("done_button")
        self.horizontalLayout_3.addWidget(self.done_button)
        self.back_button2 = QtWidgets.QPushButton(self.widget_5)
        self.back_button2.setMinimumSize(QtCore.QSize(80, 26))
        self.back_button2.setMaximumSize(QtCore.QSize(80, 26))
        self.back_button2.setStyleSheet(
            """QPushButton {padding: 4px; color:#4a4a48; border-width: 1px;
            border-style: solid; border-radius: 5px; border-color:#718355}
            QPushButton::hover {padding: 4px; color: #8e8e8c;
            border-width: 1px; border-style: solid; border-radius: 5px;}
            QPushButton:pressed {padding: 4px; color: #e9f5db;
            border-width: 1px; border-style: solid; border-radius: 5px;}"""
        )
        self.back_button2.setObjectName("back_button2")
        self.horizontalLayout_3.addWidget(self.back_button2)
        self.gridLayout_10.addWidget(self.widget_5, 27, 0, 1, 1)
        self.first_name = QtWidgets.QLabel(self.register_frame)
        self.first_name.setMinimumSize(QtCore.QSize(180, 20))
        self.first_name.setMaximumSize(QtCore.QSize(180, 20))
        self.first_name.setStyleSheet("QLabel {color: #4a4a48; border: 0px;}")
        self.first_name.setObjectName("first_name")
        self.gridLayout_10.addWidget(self.first_name, 0, 0, 1, 1)
        self.password = QtWidgets.QLabel(self.register_frame)
        self.password.setMinimumSize(QtCore.QSize(180, 20))
        self.password.setMaximumSize(QtCore.QSize(180, 20))
        self.password.setStyleSheet("QLabel {color: #4a4a48; border: 0px;}")
        self.password.setObjectName("password")
        self.gridLayout_10.addWidget(self.password, 18, 0, 1, 1)
        self.first_name_input = QtWidgets.QLineEdit(self.register_frame)
        self.first_name_input.setStyleSheet(
            """QLineEdit {padding: 4px; background-color: #e9f5db;
            border-width: 1px; border-style: solid; border-radius: 5px;
            border-color: #718355; selection-background-color: #cfe1b9;}"""
        )
        self.first_name_input.setObjectName("first_name_input")
        self.gridLayout_10.addWidget(self.first_name_input, 1, 0, 1, 1)
        self.last_name_input = QtWidgets.QLineEdit(self.register_frame)
        self.last_name_input.setStyleSheet(
            """QLineEdit {padding: 4px; background-color: #e9f5db;
            border-width: 1px; border-style: solid; border-radius: 5px;
            border-color: #718355; selection-background-color: #cfe1b9;}"""
        )
        self.last_name_input.setObjectName("last_name_input")
        self.gridLayout_10.addWidget(self.last_name_input, 9, 0, 1, 1)
        self.email_input_2 = QtWidgets.QLineEdit(self.register_frame)
        self.email_input_2.setStyleSheet(
            """QLineEdit {padding: 4px; background-color: #e9f5db;
            border-width: 1px; border-style: solid; border-radius: 5px;
            border-color: #718355; selection-background-color: #cfe1b9;}"""
        )
        self.email_input_2.setObjectName("email_input_2")
        self.gridLayout_10.addWidget(self.email_input_2, 16, 0, 1, 1)
        self.password_input_2 = QtWidgets.QLineEdit(self.register_frame)
        self.password_input_2.setStyleSheet(
            """QLineEdit {padding: 4px; background-color: #e9f5db;
            border-width: 1px; border-style: solid; border-radius: 5px;
            border-color: #718355; selection-background-color: #cfe1b9;}"""
        )
        self.password_input_2.setObjectName("password_input_2")
        self.gridLayout_10.addWidget(self.password_input_2, 19, 0, 1, 1)
        self.confirm_password_input = QtWidgets.QLineEdit(self.register_frame)
        self.confirm_password_input.setStyleSheet(
            """QLineEdit {padding: 4px; background-color: #e9f5db;
            border-width: 1px; border-style: solid; border-radius: 5px;
            border-color: #718355; selection-background-color: #cfe1b9;}"""
        )
        self.confirm_password_input.setObjectName("confirm_password_input")
        self.gridLayout_10.addWidget(self.confirm_password_input, 23, 0, 1, 1)
        self.last_name = QtWidgets.QLabel(self.register_frame)
        self.last_name.setMinimumSize(QtCore.QSize(180, 20))
        self.last_name.setMaximumSize(QtCore.QSize(180, 20))
        self.last_name.setStyleSheet("QLabel {color: #4a4a48; border: 0px;}")
        self.last_name.setObjectName("last_name")
        self.gridLayout_10.addWidget(self.last_name, 8, 0, 1, 1)
        self.email = QtWidgets.QLabel(self.register_frame)
        self.email.setMinimumSize(QtCore.QSize(180, 20))
        self.email.setMaximumSize(QtCore.QSize(180, 20))
        self.email.setStyleSheet("QLabel {color: #4a4a48; border: 0px;}")
        self.email.setObjectName("email")
        self.gridLayout_10.addWidget(self.email, 15, 0, 1, 1)
        self.confirm_password = QtWidgets.QLabel(self.register_frame)
        self.confirm_password.setMinimumSize(QtCore.QSize(180, 20))
        self.confirm_password.setMaximumSize(QtCore.QSize(180, 20))
        self.confirm_password.setStyleSheet(
            "QLabel {color: #4a4a48; border: 0px;}"
        )
        self.confirm_password.setObjectName("confirm_password")
        self.gridLayout_10.addWidget(self.confirm_password, 22, 0, 1, 1)
        self.user_exists = QtWidgets.QLabel(self.register_frame)
        self.user_exists.setMinimumSize(QtCore.QSize(0, 20))
        self.user_exists.setMaximumSize(QtCore.QSize(16777215, 20))
        self.user_exists.setStyleSheet("QLabel {color: #4a4a48; border: 0px;}")
        self.user_exists.setText("")
        self.user_exists.setAlignment(QtCore.Qt.AlignCenter)
        self.user_exists.setObjectName("user_exists")
        self.gridLayout_10.addWidget(self.user_exists, 26, 0, 1, 1)
        self.gridLayout_9.addWidget(self.register_frame, 0, 1, 1, 1)
        self.main_body.addWidget(self.register_page)
        self.view_auctions = QtWidgets.QWidget()
        self.view_auctions.setObjectName("view_auctions")
        self.gridLayout_17 = QtWidgets.QGridLayout(self.view_auctions)
        self.gridLayout_17.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_17.setSpacing(0)
        self.gridLayout_17.setObjectName("gridLayout_17")
        self.scrollArea_view_auctions = QtWidgets.QScrollArea(
            self.view_auctions
        )
        self.scrollArea_view_auctions.setStyleSheet("")
        self.scrollArea_view_auctions.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.scrollArea_view_auctions.setMidLineWidth(0)
        self.scrollArea_view_auctions.setVerticalScrollBarPolicy(
            QtCore.Qt.ScrollBarAlwaysOff
        )
        self.scrollArea_view_auctions.setHorizontalScrollBarPolicy(
            QtCore.Qt.ScrollBarAlwaysOff
        )
        self.scrollArea_view_auctions.setWidgetResizable(True)
        self.scrollArea_view_auctions.setObjectName("scrollArea_view_auctions")
        self.view_auctions_content = QtWidgets.QWidget()
        self.view_auctions_content.setGeometry(QtCore.QRect(0, 0, 750, 500))
        self.view_auctions_content.setMinimumSize(QtCore.QSize(750, 0))
        self.view_auctions_content.setObjectName("view_auctions_content")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(
            self.view_auctions_content
        )
        self.verticalLayout_2.setContentsMargins(15, 20, 15, 0)
        self.verticalLayout_2.setSpacing(20)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.scrollArea_view_auctions.setWidget(self.view_auctions_content)
        self.gridLayout_17.addWidget(self.scrollArea_view_auctions, 0, 0, 1, 1)
        self.main_body.addWidget(self.view_auctions)
        self.create_auction = QtWidgets.QWidget()
        self.create_auction.setStyleSheet("background-color: #e9f5db;")
        self.create_auction.setObjectName("create_auction")
        self.gridLayout_15 = QtWidgets.QGridLayout(self.create_auction)
        self.gridLayout_15.setObjectName("gridLayout_15")
        self.frame_2 = QtWidgets.QFrame(self.create_auction)
        self.frame_2.setMinimumSize(QtCore.QSize(700, 350))
        self.frame_2.setMaximumSize(QtCore.QSize(700, 350))
        self.frame_2.setStyleSheet(
            """QFrame {background-color: #cfe1b9; border-width: 1px;
            border-style: solid; border-color: #718355; border-radius: 5px;}"""
        )
        self.frame_2.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_2.setObjectName("frame_2")
        self.gridLayout_16 = QtWidgets.QGridLayout(self.frame_2)
        self.gridLayout_16.setObjectName("gridLayout_16")
        self.address_label = QtWidgets.QLabel(self.frame_2)
        self.address_label.setMinimumSize(QtCore.QSize(100, 0))
        self.address_label.setMaximumSize(QtCore.QSize(100, 16777215))
        self.address_label.setStyleSheet(
            "QLabel {color: #4a4a48; border: 0px;}"
        )
        self.address_label.setObjectName("address_label")
        self.gridLayout_16.addWidget(self.address_label, 1, 0, 1, 1)
        self.address_input = QtWidgets.QLineEdit(self.frame_2)
        self.address_input.setMinimumSize(QtCore.QSize(400, 0))
        self.address_input.setMaximumSize(QtCore.QSize(400, 16777215))
        self.address_input.setStyleSheet(
            """QLineEdit {padding: 4px; background-color: #e9f5db;
            border-width: 1px; border-style: solid; border-radius: 5px;
            border-color: #718355; selection-background-color: #cfe1b9;}"""
        )
        self.address_input.setObjectName("address_input")
        self.gridLayout_16.addWidget(self.address_input, 1, 1, 1, 1)
        self.city_label = QtWidgets.QLabel(self.frame_2)
        self.city_label.setStyleSheet("QLabel {color: #4a4a48; border: 0px;}")
        self.city_label.setObjectName("city_label")
        self.gridLayout_16.addWidget(self.city_label, 2, 0, 1, 1)
        self.city_input = QtWidgets.QLineEdit(self.frame_2)
        self.city_input.setMinimumSize(QtCore.QSize(400, 0))
        self.city_input.setMaximumSize(QtCore.QSize(400, 16777215))
        self.city_input.setStyleSheet(
            """QLineEdit {padding: 4px; background-color: #e9f5db;
            border-width: 1px; border-style: solid; border-radius: 5px;
            border-color: #718355; selection-background-color: #cfe1b9;}"""
        )
        self.city_input.setObjectName("city_input")
        self.gridLayout_16.addWidget(self.city_input, 2, 1, 1, 1)
        self.zip_label = QtWidgets.QLabel(self.frame_2)
        self.zip_label.setStyleSheet("QLabel {color: #4a4a48; border: 0px;}")
        self.zip_label.setObjectName("zip_label")
        self.gridLayout_16.addWidget(self.zip_label, 3, 0, 1, 1)
        self.zip_input = QtWidgets.QLineEdit(self.frame_2)
        self.zip_input.setMinimumSize(QtCore.QSize(400, 0))
        self.zip_input.setMaximumSize(QtCore.QSize(400, 16777215))
        self.zip_input.setStyleSheet(
            """QLineEdit {padding: 4px; background-color: #e9f5db;
            border-width: 1px; border-style: solid; border-radius: 5px;
            border-color: #718355; selection-background-color: #cfe1b9;}"""
        )
        self.zip_input.setObjectName("zip_input")
        self.gridLayout_16.addWidget(self.zip_input, 3, 1, 1, 1)
        self.kvm_label = QtWidgets.QLabel(self.frame_2)
        self.kvm_label.setStyleSheet("QLabel {color: #4a4a48; border: 0px;}")
        self.kvm_label.setObjectName("kvm_label")
        self.gridLayout_16.addWidget(self.kvm_label, 4, 0, 1, 1)
        self.kvm_input = QtWidgets.QLineEdit(self.frame_2)
        self.kvm_input.setMinimumSize(QtCore.QSize(400, 0))
        self.kvm_input.setMaximumSize(QtCore.QSize(400, 16777215))
        self.kvm_input.setStyleSheet(
            """QLineEdit {padding: 4px; background-color: #e9f5db;
            border-width: 1px; border-style: solid; border-radius: 5px;
            border-color: #718355; selection-background-color: #cfe1b9;}"""
        )
        self.kvm_input.setObjectName("kvm_input")
        self.gridLayout_16.addWidget(self.kvm_input, 4, 1, 1, 1)
        self.end_date_label = QtWidgets.QLabel(self.frame_2)
        self.end_date_label.setStyleSheet(
            "QLabel {color: #4a4a48; border: 0px;}"
        )
        self.end_date_label.setObjectName("end_date_label")
        self.gridLayout_16.addWidget(self.end_date_label, 5, 0, 1, 1)
        self.price_label = QtWidgets.QLabel(self.frame_2)
        self.price_label.setStyleSheet("QLabel {color: #4a4a48; border: 0px;}")
        self.price_label.setObjectName("price_label")
        self.gridLayout_16.addWidget(self.price_label, 6, 0, 1, 1)
        self.price_input = QtWidgets.QLineEdit(self.frame_2)
        self.price_input.setMinimumSize(QtCore.QSize(400, 0))
        self.price_input.setMaximumSize(QtCore.QSize(400, 16777215))
        self.price_input.setStyleSheet(
            """QLineEdit {padding: 4px; background-color: #e9f5db;
            border-width: 1px; border-style: solid; border-radius: 5px;
            border-color: #718355; selection-background-color: #cfe1b9;}"""
        )
        self.price_input.setObjectName("price_input")
        self.gridLayout_16.addWidget(self.price_input, 6, 1, 1, 1)
        self.widget_9 = QtWidgets.QWidget(self.frame_2)
        self.widget_9.setStyleSheet("background-color: #cfe1b9;")
        self.widget_9.setObjectName("widget_9")
        self.horizontalLayout_5 = QtWidgets.QHBoxLayout(self.widget_9)
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        self.submit_btn = QtWidgets.QPushButton(self.widget_9)
        self.submit_btn.setMinimumSize(QtCore.QSize(80, 26))
        self.submit_btn.setMaximumSize(QtCore.QSize(80, 26))
        self.submit_btn.setStyleSheet(
            """QPushButton {padding: 4px; color:#4a4a48; border-width: 1px;
            border-style: solid; border-radius: 5px; border-color:#718355}
            QPushButton::hover {padding: 4px; color: #8e8e8c;
            border-width: 1px; border-style: solid; border-radius: 5px;}
            QPushButton:pressed {padding: 4px; color: #e9f5db;
            border-width: 1px; border-style: solid; border-radius: 5px;}"""
        )
        self.submit_btn.setObjectName("submit_btn")
        self.horizontalLayout_5.addWidget(self.submit_btn)
        self.create_auction_back_btn = QtWidgets.QPushButton(self.widget_9)
        self.create_auction_back_btn.setMinimumSize(QtCore.QSize(80, 26))
        self.create_auction_back_btn.setMaximumSize(QtCore.QSize(80, 26))
        self.create_auction_back_btn.setStyleSheet(
            """QPushButton {padding: 4px; color:#4a4a48; border-width: 1px;
            border-style: solid; border-radius: 5px; border-color:#718355}
            QPushButton::hover {padding: 4px; color: #8e8e8c;
            border-width: 1px; border-style: solid; border-radius: 5px;}
            QPushButton:pressed {padding: 4px; color: #e9f5db;
            border-width: 1px; border-style: solid; border-radius: 5px;}"""
        )
        self.create_auction_back_btn.setObjectName("create_auction_back_btn")
        self.horizontalLayout_5.addWidget(self.create_auction_back_btn)
        self.gridLayout_16.addWidget(self.widget_9, 7, 1, 1, 1)
        self.info_label = QtWidgets.QLabel(self.frame_2)
        self.info_label.setStyleSheet("color: #4a4a48; border: 0px;")
        self.info_label.setObjectName("info_label")
        self.gridLayout_16.addWidget(self.info_label, 0, 0, 1, 2)
        self.dateTimeEdit = QtWidgets.QDateTimeEdit(self.frame_2)
        self.dateTimeEdit.setMinimumSize(QtCore.QSize(400, 26))
        self.dateTimeEdit.setMaximumSize(QtCore.QSize(400, 26))
        self.dateTimeEdit.setStyleSheet(
            """QDateTimeEdit {padding: 4px; background-color: #e9f5db;
            border-width: 1px; border-style: solid; border-radius: 5px;
            border-color: #718355; selection-background-color: #cfe1b9;}"""
        )
        self.dateTimeEdit.setButtonSymbols(QtWidgets.QAbstractSpinBox.NoButtons)
        self.dateTimeEdit.setMaximumDateTime(
            QtCore.QDateTime(QtCore.QDate(9999, 12, 31), QtCore.QTime(12, 0, 0))
        )
        self.dateTimeEdit.setCalendarPopup(True)
        self.dateTimeEdit.setObjectName("dateTimeEdit")
        self.gridLayout_16.addWidget(self.dateTimeEdit, 5, 1, 1, 1)
        self.gridLayout_15.addWidget(self.frame_2, 0, 0, 1, 1)
        self.main_body.addWidget(self.create_auction)
        self.finished_auctions = QtWidgets.QWidget()
        self.finished_auctions.setObjectName("finished_auctions")
        self.gridLayout = QtWidgets.QGridLayout(self.finished_auctions)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setSpacing(0)
        self.gridLayout.setObjectName("gridLayout")
        self.scrollArea_finished_auctions = QtWidgets.QScrollArea(
            self.finished_auctions
        )
        self.scrollArea_finished_auctions.setStyleSheet("")
        self.scrollArea_finished_auctions.setFrameShape(
            QtWidgets.QFrame.NoFrame
        )
        self.scrollArea_finished_auctions.setVerticalScrollBarPolicy(
            QtCore.Qt.ScrollBarAlwaysOff
        )
        self.scrollArea_finished_auctions.setHorizontalScrollBarPolicy(
            QtCore.Qt.ScrollBarAlwaysOff
        )
        self.scrollArea_finished_auctions.setWidgetResizable(True)
        self.scrollArea_finished_auctions.setObjectName(
            "scrollArea_finished_auctions"
        )
        self.finished_auctions_content = QtWidgets.QWidget()
        self.finished_auctions_content.setGeometry(QtCore.QRect(0, 0, 750, 500))
        self.finished_auctions_content.setMinimumSize(QtCore.QSize(750, 0))
        self.finished_auctions_content.setObjectName(
            "finished_auctions_content"
        )
        self.verticalLayout_6 = QtWidgets.QVBoxLayout(
            self.finished_auctions_content
        )
        self.verticalLayout_6.setContentsMargins(15, 20, 15, 0)
        self.verticalLayout_6.setSpacing(20)
        self.verticalLayout_6.setObjectName("verticalLayout_6")
        self.scrollArea_finished_auctions.setWidget(
            self.finished_auctions_content
        )
        self.gridLayout.addWidget(self.scrollArea_finished_auctions, 0, 0, 1, 1)
        self.main_body.addWidget(self.finished_auctions)
        self.my_auctions_2 = QtWidgets.QWidget()
        self.my_auctions_2.setObjectName("my_auctions_2")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.my_auctions_2)
        self.gridLayout_2.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_2.setSpacing(0)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.scrollArea = QtWidgets.QScrollArea(self.my_auctions_2)
        self.scrollArea.setFrameShape(QtWidgets.QFrame.Box)
        self.scrollArea.setVerticalScrollBarPolicy(
            QtCore.Qt.ScrollBarAlwaysOff
        )
        self.scrollArea.setHorizontalScrollBarPolicy(
            QtCore.Qt.ScrollBarAlwaysOff
        )
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setObjectName("scrollArea")
        self.my_auctions_content = QtWidgets.QWidget()
        self.my_auctions_content.setGeometry(QtCore.QRect(0, 0, 750, 500))
        self.my_auctions_content.setMinimumSize(QtCore.QSize(750, 0))
        self.my_auctions_content.setObjectName("my_auctions_content")
        self.verticalLayout_5 = QtWidgets.QVBoxLayout(self.my_auctions_content)
        self.verticalLayout_5.setContentsMargins(15, 20, 15, 0)
        self.verticalLayout_5.setSpacing(20)
        self.verticalLayout_5.setObjectName("verticalLayout_5")
        self.scrollArea.setWidget(self.my_auctions_content)
        self.gridLayout_2.addWidget(self.scrollArea, 0, 0, 1, 1)
        self.main_body.addWidget(self.my_auctions_2)
        self.bid_page = QtWidgets.QWidget()
        self.bid_page.setObjectName("bid_page")
        self.verticalLayout_8 = QtWidgets.QVBoxLayout(self.bid_page)
        self.verticalLayout_8.setContentsMargins(15, 20, 15, 100)
        self.verticalLayout_8.setSpacing(20)
        self.verticalLayout_8.setObjectName("verticalLayout_8")
        self.bid_info = QtWidgets.QLabel(self.bid_page)
        self.bid_info.setText("")
        self.bid_info.setAlignment(QtCore.Qt.AlignCenter)
        self.bid_info.setObjectName("bid_info")
        self.verticalLayout_8.addWidget(self.bid_info, 0, QtCore.Qt.AlignTop)
        self.main_body.addWidget(self.bid_page)
        self.bodyGridLayout.addWidget(self.main_body, 0, 0, 1, 1)
        self.mainBodyLayout.addWidget(self.body_frame)
        self.centralGridLayout.addWidget(self.main_body_frame, 1, 0, 2, 1)

        self.setCentralWidget(self.centralwidget)
        self.set_texts()
        self.stacked_top.setCurrentIndex(0)
        self.stackedWidget.setCurrentIndex(1)
        self.main_body.setCurrentIndex(0)

    # -- Custom code goes here --
    # -- VARIABLES --
        self.db_file = Path.cwd() / 'dbdata.db'
        self.user = ''
        self.auctionObjects = {}
        self.current_object = None
        self.widgets = []
        self.todays_date = datetime.today().strftime('%Y-%m-%d %H:%M:%S')
        self.ten_days = date.today() + timedelta(days=10)
        y = int(str(self.ten_days)[0:4])
        m = int(str(self.ten_days)[5:7])
        d = int(str(self.ten_days)[8:])
        self.dateTimeEdit.setMinimumDateTime(
            QtCore.QDateTime(QtCore.QDate(y, m, d), QtCore.QTime(12, 0, 0))
        )
    # -- VARIABLES --

    # -- Login and register buttons --
        self.login.clicked.connect(self.loginPage)
        self.register_2.clicked.connect(self.registerPage)
    # -- END Login and register buttons --

    # -- Login page logic --
        self.sign_in.clicked.connect(self.userLogin)
        self.back_button.clicked.connect(self.logoutAction)
    # -- END Login page logic --

    # -- Register page logic --
        self.done_button.clicked.connect(self.userRegister)
        self.back_button2.clicked.connect(self.generalPage)
    # -- END Register page logic --

    # -- Logout button --
        self.logout_btn.clicked.connect(self.logoutAction)
    # -- END Logout button --

    # -- Create auctions button --
        self.create_auction_btn.clicked.connect(self.createAuctionsPage)
        self.submit_btn.clicked.connect(self.createAuctionItem)
        self.create_auction_back_btn.clicked.connect(self.clearAllAuctionFields)
    # -- END Create auctions button --

    # -- View auctions --
        self.view_auctions_btn.clicked.connect(self.viewAuctionsPage)
        self.pushButton.clicked.connect(self.finishedAuctionsPage)
    # -- END View auctions --

    # -- My auctions button --
        self.my_auctions_btn.clicked.connect(self.myAuctionsPage)

    # -- Home button --
        self.home_btn.clicked.connect(self.generalPage)
    # -- END Home button --

        self.initAuctions()
        self.initFinishedAuctions()
        self.updateBidBtn()
    # -- TEST --
    # -- TEST --

    # -- Main body pages --
    def generalPage(self):
        self.main_body.setCurrentIndex(0)
        if self.current_object:
            self.current_object.close()
            self.bid_info.setText("")

    def loginPage(self):
        self.main_body.setCurrentIndex(1)

    def registerPage(self):
        self.main_body.setCurrentIndex(2)

    def viewAuctionsPage(self):
        self.main_body.setCurrentIndex(3)
        if self.current_object:
            self.current_object.close()
            self.bid_info.setText("")

    def createAuctionsPage(self):
        self.main_body.setCurrentIndex(4)

    def finishedAuctionsPage(self):
        self.main_body.setCurrentIndex(5)

    def myAuctionsPage(self):
        self.main_body.setCurrentIndex(6)

    # -- END Main body pages --

    # -- User login logic --

    def userLogin(self):
        self.user_email = self.email_input.text()
        user_password = self.password_input.text()
        if len(self.user_email) > 0 and len(user_password) > 0:
            try:
                conn = sqlite3.connect(self.db_file)
                cur = conn.cursor()
                cur.execute('''SELECT user_account.email,
                            user_account.password, user_account.first_name,
                            user_account.last_name, user_account.balance
                            FROM user_account
                            WHERE email = ? AND password = ?;''',
                            (self.user_email, user_password))
                response = cur.fetchall()
                if response:
                    self.user = f'''{str(response[0][2])} {str(response[0][3])}'''
                    self.balance = response[0][-1]
                    self.balance_label.setText(str(self.balance))
                    self.stackedWidget.setCurrentIndex(0)
                    self.main_body.setCurrentIndex(0)
                    self.current_user.setText(self.user)
                    print(self.user)
                    self.stacked_top.setCurrentIndex(1)
                    self.email_error.setText('')
                    self.initMyAuctions()
                    conn.close()
                else:
                    self.email_error.setText(
                        '     Incorrect email or password'
                    )
                    self.stacked_top.setCurrentIndex(0)
                    conn.close()
            except Error as er:
                print(er)

    # -- END User login logic --

    # -- My auctions logic --
    def initMyAuctions(self):
        try:
            conn = sqlite3.connect(self.db_file)
            cur = conn.cursor()
            cur.execute('''SELECT sales_object.street_adress,
                        sales_object.zip_code, sales_object.city,
                        sales_object.uuid, auction.created, auction.end_date,
                        sales_object.square_meter, auction.current_price
                        FROM auction JOIN sales_object
                        ON auction.uuid = sales_object.uuid
                        WHERE auction.owner = ?;''', (self.user_email,))
            response = cur.fetchall()
            for i in response:
                address = f'{i[0]},  {i[1]},  {i[2]}'
                self.w = self.myAuction(i[3], address, i[4], i[5], i[-1], i[-2])
                self.widgets.append(self.w)
        except Error as er:
            print(er)

    def myAuction(self, _id, _adrs, _date_time, _end_date, _price, _kvm):
        auction = newAuction(self.my_auctions_content)
        today = str(datetime.today())
        today_string = str(today).replace(
            str(today)[str(today).rfind(':'):], ''
        )
        expiration = datetime.strptime((_end_date), '%Y-%m-%d %H:%M')
        now = datetime.strptime(today_string, '%Y-%m-%d %H:%M')
        if expiration < now:
            auction.auctionID_iLabel.setText(str(_id))
            auction.address_iLabel.setText(str(_adrs))
            auction.date_created_iLabel.setText(str(_date_time))
            auction.end_date_iLabel.setText('Expired')
            auction.price_label_2.setText('Winning bid:')
            auction.price_iLabel.setText(str(_price))
            auction.kvm_iLabel.setText(str(_kvm))
            self.verticalLayout_5.addWidget(auction)
            return auction
        else:
            auction.auctionID_iLabel.setText(str(_id))
            auction.address_iLabel.setText(str(_adrs))
            auction.date_created_iLabel.setText(str(_date_time))
            auction.end_date_iLabel.setText(str(_end_date))
            auction.price_iLabel.setText(str(_price))
            auction.kvm_iLabel.setText(str(_kvm))
            self.verticalLayout_5.addWidget(auction)
            return auction

    # -- END My auctions logic --

    # -- User register logic --
    def userRegister(self):
        a = self.first_name_input.text()
        b = self.last_name_input.text()
        c = self.email_input_2.text()
        d = self.password_input_2.text()
        e = self.confirm_password_input.text()
        if len(a) > 0 and len(b) > 0 and len(
            c
        ) > 0 and len(d) > 0 and len(e) > 0:
            if d == e:
                user = [
                    self.first_name_input.text(), self.last_name_input.text(),
                    self.email_input_2.text(), self.password_input_2.text()
                ]
                starting_amount = 50000000
                self.user_email = user[2]
                try:
                    conn = sqlite3.connect(self.db_file)
                    cur = conn.cursor()
                    cur.execute('''SELECT user_account.email
                                FROM user_account
                                WHERE email = ?;''',
                                (user[2],))
                    if cur.fetchall():
                        self.user_exists.setText('User with this email exists')
                        conn.close()
                    else:
                        cur.execute('''INSERT INTO user_account(email,
                                    password, first_name, last_name,
                                    balance)
                                    VALUES(?, ?, ?, ?, ?);''',
                                    (user[2], user[3], user[0], user[1],
                                        starting_amount)
                                    )
                        conn.commit()
                        conn.close()
                        self.user = f'{user[0]} {user[1]}'
                        self.balance = starting_amount
                        self.balance_label.setText(str(self.balance))
                        self.stacked_top.setCurrentIndex(1)
                        self.stackedWidget.setCurrentIndex(0)
                        self.main_body.setCurrentIndex(0)
                        self.user_exists.setText('')
                        self.initMyAuctions()
                        self.current_user.setText(self.user)
                except Error as er:
                    print(er)
            else:
                self.user_exists.setText("Password doesn't match")
    # -- User register logic --

    # -- Logout action --
    def logoutAction(self):
        self.stacked_top.setCurrentIndex(0)
        self.stackedWidget.setCurrentIndex(1)
        self.main_body.setCurrentIndex(0)
        self.email_input.clear()
        self.password_input.clear()
        self.first_name_input.clear()
        self.last_name_input.clear()
        self.email_input_2.clear()
        self.password_input_2.clear()
        self.confirm_password_input.clear()
        self.balance = None
        self.user_email = None
        self.email_error.setText('')
        if self.widgets:
            for i in self.widgets:
                i.close()

        self.user = ''
        if self.current_object:
            self.current_object.close()

    # -- END Logout action --

    # -- Bidding logic --
    def updateBidBtn(self):
        for i in self.auctionObjects:
            i.bid_btn.clicked.connect(self.startBid)

    def startBid(self):
        for i in self.auctionObjects:
            if i.selected:
                self.id = self.auctionObjects[i][0]
                self.sender = i
                self.main_body.setCurrentIndex(7)
                try:
                    conn = sqlite3.connect(self.db_file)
                    cur = conn.cursor()
                    cur.execute('''SELECT ongoing_auction.uuid,
                    ongoing_auction.current_leader, ongoing_auction.leading_bid
                    FROM ongoing_auction
                    WHERE ongoing_auction.uuid = ?;''', (self.id,))
                    response = cur.fetchall()
                    if response:
                        self.current_object = self.bidAuction(
                            self.auctionObjects[i][1],
                            self.auctionObjects[i][2],
                            self.auctionObjects[i][6],
                            self.auctionObjects[i][4],
                            self.auctionObjects[i][5],
                            response[0][1], response[0][2]
                        )
                        self.current_object.submit_bid.clicked.connect(
                            self.submitBid
                        )
                        i.selected = False
                    else:
                        self.current_object = self.bidAuction(
                            self.auctionObjects[i][1],
                            self.auctionObjects[i][2],
                            self.auctionObjects[i][6],
                            self.auctionObjects[i][4],
                            self.auctionObjects[i][5],
                            _bid=self.auctionObjects[i][5]
                        )
                        self.current_object.submit_bid.clicked.connect(
                            self.submitBid
                        )
                        i.selected = False
                except Error as er:
                    print(er)

    def updateOngoingAuction(self, _key, _bid):
        """
        This function takes information from Database about ongoing auction and
        creates a widget where the user can put down bids
        """
        try:
            conn = sqlite3.connect(self.db_file)
            cur = conn.cursor()
            cur.execute('''UPDATE auction SET current_price = ?
                        WHERE auction.uuid = ?;''', (_bid, self.id))
            cur.execute('''UPDATE ongoing_auction
                       SET current_leader = ?, leading_bid =?
                       WHERE ongoing_auction.uuid = ?;''',
                        (self.user, _bid, self.id))
            _key.price_iLabel.setText(_bid)
            self.current_object.close()
            self.main_body.setCurrentIndex(0)
            conn.commit()
            conn.close()
        except Error as er:
            print(er)

    def addToOngoing(self, _key, _bid):
        """
        This function creates a new widget and simultaneously adds this
        information to the table 'ongoing_auction' in Database
        """
        auction = self.auctionObjects[_key]
        try:
            conn = sqlite3.connect(self.db_file)
            cur = conn.cursor()
            cur.execute('''INSERT INTO ongoing_auction (uuid, owner, address,
                        square_meter, created, end_date, current_leader,
                        leading_bid)
                        VALUES (?,?,?,?,?,?,?,?);''', (auction[0],
                        auction[-1], auction[2], auction[6], auction[3],
                        auction[4], self.user, _bid))
            cur.execute('''UPDATE auction SET current_price = ?
                        WHERE auction.uuid = ?;''', (_bid, self.id))
            _key.price_iLabel.setText(_bid)
            self.current_object.close()
            self.main_body.setCurrentIndex(0)
            conn.commit()
            conn.close()
        except Error as er:
            print(er)

    def submitBid(self):
        """auction.current_price"""
        today = str(datetime.today())
        today_string = str(today).\
            replace(str(today)[str(today).rfind(':'):], '')
        try:
            conn = sqlite3.connect(self.db_file)
            cur = conn.cursor()
            cur.execute('''SELECT auction.uuid, auction.current_price,
                           auction.owner
                           FROM auction
                           WHERE auction.uuid = ?;''', (self.id, ))
            response = cur.fetchall()
            current_price = response[0][1]
            owner = response[0][2]
            uuid = response[0][0]
            self.current_object.leading_bid_iLabel.setText(str(current_price))
            conn.close()
        except Error as er:
            print('This', er)
        if owner == self.user_email:
            self.bid_info.setText('You cannot bid on your own objects...')
        else:
            bid = self.current_object.bid_insert_lineEdit.text()
            if len(bid) > 0 and int(bid) > current_price and int(
                bid
            ) < self.balance:
                if self.isThisActive(self.id):
                    self.updateBalance()
                    self.updateOngoingAuction(self.sender, bid)
                    # Send info to history
                    self.addToHistory(uuid, self.user_email, bid, today_string)
                else:
                    self.updateBalance()
                    self.addToOngoing(self.sender, bid)
                    self.addToHistory(uuid, self.user_email, bid, today_string)
            else:
                self.bid_info.setText(
                    'Your bid must be higher than the current price'
                )

    def addToHistory(self, _uuid, _email, _bid, _today_string):
        try:
            conn = sqlite3.connect(self.db_file)
            cur = conn.cursor()
            cur.execute('''INSERT INTO bid_history (auction_uuid,
                        bidder, bid, bid_time)
                        VALUES (?, ?, ?, ?)''',
                        (_uuid, _email, _bid, _today_string, ))
            conn.commit()
            conn.close()
        except Error as er:
            print(er)

    def updateBalance(self):
        bid = self.current_object.bid_insert_lineEdit.text()
        updated_balance = self.balance - int(bid)
        try:
            conn = sqlite3.connect(self.db_file)
            cur = conn.cursor()
            cur.execute('''UPDATE user_account SET balance = ?
                        WHERE user_account.email = ?;''', (updated_balance,
                                                           self.user_email,))
            self.balance_label.setText(str(updated_balance))
            conn.commit()
            conn.close()
        except Error as er:
            print(er)

    def isThisActive(self, _id):
        try:
            conn = sqlite3.connect(self.db_file)
            cur = conn.cursor()
            cur.execute('''SELECT ongoing_auction.uuid FROM ongoing_auction
                           WHERE ongoing_auction.uuid = ?;''', (_id,))
            response = cur.fetchall()
            if response:
                return True
                conn.close()
            else:
                return False
                conn.close()
        except Error as er:
            print('THIS', er)

    def bidAuction(self, _owner, _address, _kvm, _end_date, _current_price,
                   _leader=None, _bid=None):
        bid_auction = bidWidget(self.bid_page)
        bid_auction.owner_iLabel.setText(str(_owner))
        bid_auction.address_iLabel.setText(str(_address))
        bid_auction.kvm_iLabel.setText(str(_kvm))
        bid_auction.created_iLabel.setText(str(_end_date))
        bid_auction.end_date_iLabel.setText(str(_current_price))
        bid_auction.bid_leader_iLabel.setText(str(_leader))
        bid_auction.leading_bid_iLabel.setText(str(_bid))
        self.verticalLayout_8.addWidget(bid_auction)
        return bid_auction

    # -- END Bidding logic --

    # -- Create auction item --
    def createAuctionItem(self):
        """
        sales_object:
        uuid::
        address = object[0]
        city = object[1]
        zip = object[2]
        square_meter = object[3]

        auction:
        uuid::
        owner = self.user
        date_created = self.todays_date
        end date = object[4]
        starting price = object[5]
        current price = starting price
        """
        a = self.address_input.text()
        b = self.city_input.text()
        c = self.zip_input.text()
        d = self.kvm_input.text()
        e = self.dateTimeEdit.text()
        f = self.price_input.text()
        if len(a) > 0 and len(b) > 0 and len(c) == 5 and len(
            d
        ) > 0 and len(e) > 0 and len(f) > 0:
            if self.price_input.text().isdigit():
                object = [
                    self.address_input.text(), self.city_input.text(),
                    self.zip_input.text(), self.kvm_input.text(),
                    self.dateTimeEdit.text(), self.price_input.text()
                ]
                try:
                    conn = sqlite3.connect(self.db_file)
                    cur = conn.cursor()
                    cur.execute('''SELECT sales_object.street_adress,
                                sales_object.zip_code, sales_object.city
                                FROM sales_object
                                WHERE sales_object.zip_code = ?
                                AND sales_object.street_adress = ?;''',
                                (object[2], object[0]))
                    if not cur.fetchall():
                        uuid_auction = str(uuid.uuid4())
                        address = f'{object[0]}  {object[2]}  {object[1]}'
                        today = str(datetime.today())
                        today_string = str(today).\
                            replace(str(today)[str(today).rfind(':'):], '')
                        cur.execute('''INSERT INTO sales_object(uuid,
                                    street_adress, city, zip_code, square_meter)
                                    VALUES(?, ?, ?, ?, ?);''',
                                    (uuid_auction, object[0], object[1],
                                        object[2], object[3]))
                        cur.execute('''INSERT INTO auction(uuid, owner, created,
                                    end_date, starting_price, current_price)
                                    VALUES(?, ?, ?, ?, ?, ?);''',
                                    (uuid_auction, self.user_email,
                                        today_string, object[4],
                                        object[5], object[5]))
                        conn.commit()
                        conn.close()
                        self.auctionObjects[
                            self.addAuction(uuid_auction, self.user, address,
                                            today_string, object[4],
                                            object[5], object[3]
                                            )
                        ] = [uuid_auction, self.user, address, today_string,
                             object[4], object[5], object[3], self.user_email]
                        self.updateBidBtn()
                        self.clearAllAuctionFields()
                        self.initMyAuctions()
                        if self.current_object:
                            self.current_object.close()

                        self.info_label.setText('''Please fill in valid
                        information about your desired object''')
                    else:
                        self.info_label.setText('''Auction object
                                                already exists...''')
                except Error as er:
                    print(er)
            else:
                self.info_label.setText('''Starting price must
                                        be written in numbers...''')

    # -- END Create auction item --

    # -- Initializing auction items --
    def initAuctions(self):
        try:
            conn = sqlite3.connect(self.db_file)
            cur = conn.cursor()
            cur.execute('''SELECT auction.uuid, auction.owner,
                        sales_object.street_adress, auction.created,
                        auction.end_date, auction.current_price,
                        sales_object.square_meter, sales_object.city,
                        sales_object.zip_code,
                        user_account.first_name, user_account.last_name
                        FROM sales_object
                        JOIN auction
                        JOIN user_account
                        ON sales_object.uuid = auction.uuid
                        AND auction.owner = user_account.email;''')
            response = cur.fetchall()
            for i in response:
                today = str(datetime.today())
                today_string = str(today).replace(
                    str(today)[str(today).rfind(':'):], ''
                )
                expiration = datetime.strptime(str(i[4]), '%Y-%m-%d %H:%M')
                now = datetime.strptime(today_string, '%Y-%m-%d %H:%M')
                if expiration > now:
                    username = f'{i[-2]} {i[-1]}'
                    address = f'{i[2]},  {i[8]},  {i[7]}'
                    self.auctionObjects[
                        self.addAuction(i[0], username, address,
                                        i[3], i[4], i[5], i[6])
                    ] = [i[0], i[1], i[2], i[3], i[4], i[5], i[6], i[7], i[8]]
                    conn.close()
                else:
                    conn = sqlite3.connect(self.db_file)
                    cur = conn.cursor()
                    cur.execute('''SELECT ongoing_auction.owner,
                                ongoing_auction.current_leader,
                                ongoing_auction.leading_bid,
                                auction.starting_price
                                FROM ongoing_auction
                                JOIN auction
                                WHERE ongoing_auction.uuid = ?
                                AND auction.uuid = ?;''', (i[0], i[0]))
                    res = cur.fetchall()
                    if res and not self.isInDb(i[0]):
                        total_winnings = (int(res[0][2]) - int(res[0][3]))
                        cur.execute('''INSERT INTO finished_auctions(uuid,
                                    owner, winner, winning_bid, total_winnings)
                                    VALUES (?,?,?,?,?)''', (i[0], res[0][0],
                                                            res[0][1],
                                                            res[0][2],
                                                            total_winnings,))
                        conn.commit()
                        conn.close()
                        self.deleteFromOngoing(i[0])
        except Error as er:
            print(er)

    def deleteFromOngoing(self, _id):
        try:
            conn = sqlite3.connect(self.db_file)
            cur = conn.cursor()
            cur.execute('''DELETE FROM ongoing_auction
                        WHERE ongoing_auction.uuid = ?;''', (_id,))
            conn.commit()
            conn.close()
        except Error as er:
            print(er)

    def isInDb(self, _id):
        try:
            conn = sqlite3.connect(self.db_file)
            cur = conn.cursor()
            cur.execute('''SELECT finished_auctions.uuid
                        FROM finished_auctions
                        WHERE finished_auctions.uuid = ?;''', (_id,))
            if cur.fetchall():
                return True
            else:
                return False
        except Error as er:
            print(er)

    def addAuction(self, _id, _owner, _adrs, _date, _end_date, _price, _kvm):
        auction = widgetWithButton(self.view_auctions_content)
        auction.auction_iLabel.setText(str(_id))
        auction.owner_iLabel.setText(str(_owner))
        auction.address_iLabel.setText(_adrs)
        auction.date_created_iLabel.setText(str(_date))
        auction.end_date_iLabel.setText(str(_end_date))
        auction.price_iLabel.setText(str(_price))
        auction.kvm_iLabel.setText(str(_kvm))
        self.verticalLayout_2.addWidget(auction)
        return auction

    # -- END Initializing auction items --

    # -- Initializing finished auctions --
    def initFinishedAuctions(self):
        try:
            conn = sqlite3.connect(self.db_file)
            cur = conn.cursor()
            cur.execute('''SELECT finished_auctions.uuid,
                        finished_auctions.owner, finished_auctions.winner,
                        finished_auctions.winning_bid,
                        finished_auctions.total_winnings,
                        user_account.first_name, user_account.last_name
                        FROM finished_auctions
                        JOIN user_account
                        WHERE finished_auctions.owner = user_account.email;''')
            response = cur.fetchall()
            if response:
                for i in response:
                    owner = f'{response[0][-2]} {response[0][-1]}'
                    self.finishedAuction(owner, i[2], i[3], i[0], i[4])
                    self.distributeCredit(i[4], i[1])
        except Error as er:
            print(er)

    def distributeCredit(self, _total_winnings, _email):
        try:
            conn = sqlite3.connect(self.db_file)
            cur = conn.cursor()
            cur.execute('''SELECT user_account.balance, user_account.email
                        FROM user_account;''')
            r = cur.fetchall()
            if r:

                users = len(r)
                credits = _total_winnings // users
                for i in list(r):
                    current = list(i)[0]
                    new = current + credits
                    user = list(i)[1]
                    cur.execute('''UPDATE user_account SET balance = ?
                                WHERE user_account.balance = ?
                                AND user_account.email = ?;''',
                                (new, current, user,))
                    conn.commit()
        except Error as er:
            print(er)

    def finishedAuction(self, _owner, _winner, _win_bid, _id, _winnings):
        fininshed_auction = finishedAuction(self.finished_auctions_content)
        fininshed_auction.owner_iLabel.setText(str(_owner))
        fininshed_auction.winner_iLabel.setText(str(_winner))
        fininshed_auction.winning_iLabel.setText(str(_win_bid))
        fininshed_auction.finished_auction_iLabel.setText(str(_id))
        fininshed_auction.total_winnings_iLabel.setText(str(_winnings))
        self.verticalLayout_6.addWidget(fininshed_auction)

    # -- END Initializing finished auctions --

    # -- Clear create auction fields --
    def clearAllAuctionFields(self):
        self.address_input.clear()
        self.city_input.clear()
        self.zip_input.clear()
        self.kvm_input.clear()
        self.dateTimeEdit.clear()
        self.price_input.clear()
        self.generalPage()

    # -- END Clear create auction fields --

    # -- END Custom code goes here --

    def set_texts(self):
        self.setWindowTitle("MainWindow")
        self.login.setText("Login")
        self.register_2.setText("Register")
        self.logo_label.setText("J.K Auction")
        self.create_auction_btn.setText("Create Auction")
        self.view_auctions_btn.setText("Ongoing Auctions")
        self.pushButton.setText("Finished auctions")
        self.home_btn.setText("Home")
        self.logout_btn.setText("Logout")
        self.my_auctions_btn.setText("My Auctions")
        self.textBrowser.setHtml("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><meta charset=\"utf-8\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'.AppleSystemUIFont\'; font-size:13pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Welcome to the worlds first \'Aucker\'! A concept in which the functions of auctions and poker are mixed together to form a brilliant new \'everyone wins\' concept.</p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">How to participate:</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Step 1: Register an account to use when you bid on properties. (If you already have an account, just login...)</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Step 2: Create an auction object. This will be an address of your choosing (Be careful since we don\'t actually buy and sell properties, don\'t use actual properties)</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Step 3: Bid on auctions. When you register an account you get 50 000 000 credits for free to bid on diffrent properties. Spend, spend, spend dawg.</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Step 4: Wait for the auctions to expire at the \'End date\' for it to finish.</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Step 5: Profit</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"> </p></body></html>")
        self.password_input.setWhatsThis("<html><head/><body><p><br/></p></body></html>")
        self.password_label.setText("   Password:")
        self.email_label.setText("   Email:")
        self.sign_in.setText("Sign in")
        self.back_button.setText("Back")
        self.done_button.setText("Done")
        self.back_button2.setText("Back")
        self.first_name.setText("   First name:")
        self.password.setText("   Password:")
        self.last_name.setText("   Last name:")
        self.email.setText("   Email:")
        self.confirm_password.setText("   Confirm password:")
        self.address_label.setText("Street adress:")
        self.city_label.setText("City:")
        self.zip_label.setText("Zip code:")
        self.kvm_label.setText("Square meters:")
        self.end_date_label.setText("End date for sale:")
        self.price_label.setText("Starting price:")
        self.submit_btn.setText("Submit")
        self.create_auction_back_btn.setText("Back")
        self.info_label.setText(
            "Please fill in valid information about your desired object"
        )


if __name__ == "__main__":
    import sys
    from my_widgets import newAuction, finishedAuction, widgetWithButton, bidWidget
    from pathlib import Path
    import sqlite3
    from sqlite3 import Error
    import uuid
    from datetime import timedelta, date, datetime
    app = QtWidgets.QApplication(sys.argv)
    ui = Ui_MainWindow()
    ui.show()
    sys.exit(app.exec())
else:
    import sys
    from .my_widgets import newAuction, finishedAuction, widgetWithButton, bidWidget
    from pathlib import Path
    import sqlite3
    from sqlite3 import Error
    import uuid
    from datetime import timedelta, date, datetime
