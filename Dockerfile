FROM jozo/pyqt5:latest

WORKDIR /app

ADD src/ /app/src

ADD requirements.txt .

ADD dbdata.db .

CMD [ "python3", "src/interface.py"]