--SQLite3 Schema and seeds
--to simply recreate the database. Run below commands in the terminal.
-- rm dbdata.db
-- cat Schema_seeds_u06_database.sql | sqlite3 dbdata.db
--
--For prettier sqlite3 output type below command in the terminal.
-- sqlite3 -box -header dbdata.db

BEGIN TRANSACTION;

CREATE TABLE user_account (id INTEGER PRIMARY KEY AUTOINCREMENT,
                           email TEXT NOT NULL,
                           password TEXT NOT NULL,
                           first_name TEXT NOT NULL,
                           last_name TEXT NOT NULL,
                           user_level TEXT DEFAULT 'user' NOT NULL,
                           balance INTEGER,
                           created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP);


INSERT INTO user_account (email, password, first_name, last_name, user_level, balance, created) VALUES("viktor.berg@chasacademy.se", "5uperMegapasswd", "Viktor", "Berg", 'admin', 400000000, "2022-02-15 13:56");
INSERT INTO user_account (email, password, first_name, last_name, user_level, balance, created) VALUES("tosh.jhomsson@lhuv.se", "Ub3rdub3rpasswd", "Tosh", "Jhomsson", 'user', 400000, "2022-02-16 02:34");
INSERT INTO user_account (email, password, first_name, last_name, user_level, balance, created) VALUES("love.murphy@lhuv.se", "133sju", "Love", "Murphy", 'user', 400000, "2022-03-18 07:44");
INSERT INTO user_account (email, password, first_name, last_name, user_level, balance, created) VALUES("michael.scott@dundermifflin.com", "1234", "Michael", "Scott", 'user', 400000, "2022-03-18 09:33");
INSERT INTO user_account (email, password, first_name, last_name, user_level, balance, created) VALUES("no.emagination@left.com", "password", "No", "Emagination", "user", 400000, "2022-04-20 12:34");


CREATE TABLE sales_object (uuid TEXT PRIMARY KEY NOT NULL,
                           street_adress TEXT NOT NULL,
                           city TEXT NOT NULL,
                           zip_code TEXT NOT NULL,
                           square_meter INTEGER NOT NULL);

INSERT INTO sales_object (uuid, street_adress, city, zip_code, square_meter) VALUES ("4c90c613-4102-42c8-8df1-016a95edfe4f", "Fifth st.", "Borås", "50334", "136");
INSERT INTO sales_object (uuid, street_adress, city, zip_code, square_meter) VALUES ("3f80d8e3-3de8-4245-a2f8-0c8ca488a6d2", "Thirtyfifth st.", "Kumla", "88837", "112");
INSERT INTO sales_object (uuid, street_adress, city, zip_code, square_meter) VALUES ("3791ea4b-1ccf-45da-9676-284aa2df6bc9", "126 Kellum Court", "Scranton", "18510", "1458");
INSERT INTO sales_object (uuid, street_adress, city, zip_code, square_meter) VALUES ("80cd4c8f-8721-4e31-bf20-e9ac9c3c29bc", "Some Made Up Place 42", "Neverland", "99999", "12");
INSERT INTO sales_object (uuid, street_adress, city, zip_code, square_meter) VALUES ("52b3868d-9de4-4e75-ae9a-28bca17c535f", "45 Gravel st.", "Gravel Town", "98777", "99");
INSERT INTO sales_object (uuid, street_adress, city, zip_code, square_meter) VALUES ("867ed72d-3fa9-4621-bfd0-3ca7b2b64075", "22 Jolt st.", "Krown", "22233", "199");
INSERT INTO sales_object (uuid, street_adress, city, zip_code, square_meter) VALUES ("18f1d168-01a7-4e06-953a-10d476d9091c", "45 No More Ideas st.", "Nowhere", "44783", "333");



CREATE TABLE auction (uuid TEXT PRIMARY KEY NOT NULL,
                      owner TEXT NOT NULL,
                      created DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
                      end_date DATETIME NOT NULL,
                      starting_price INTEGER NOT NULL,
                      current_price INTEGER NOT NULL);

INSERT INTO auction (uuid, owner, created, end_date, starting_price, current_price) VALUES ("4c90c613-4102-42c8-8df1-016a95edfe4f","viktor.berg@chasacademy.se","2022-05-05 15:55","2022-05-15 12:00",1200000,1200000);
INSERT INTO auction (uuid, owner, created, end_date, starting_price, current_price) VALUES ("3f80d8e3-3de8-4245-a2f8-0c8ca488a6d2","viktor.berg@chasacademy.se","2022-05-05 16:44","2022-05-15 12:00",2500000,2500000);
INSERT INTO auction (uuid, owner, created, end_date, starting_price, current_price) VALUES ("3791ea4b-1ccf-45da-9676-284aa2df6bc9","michael.scott@dundermifflin.com","2022-05-08 10:15","2022-05-18 12:00",700000,700000);
INSERT INTO auction (uuid, owner, created, end_date, starting_price, current_price) VALUES ("80cd4c8f-8721-4e31-bf20-e9ac9c3c29bc","no.emagination@left.com","2022-05-11 08:25","2022-05-21 12:00",4800,4800);
INSERT INTO auction (uuid, owner, created, end_date, starting_price, current_price) VALUES ("52b3868d-9de4-4e75-ae9a-28bca17c535f","tosh.jhomsson@lhuv.se","2022-05-07 07:33","2022-05-17 12:00",1800000,1800000);
INSERT INTO auction (uuid, owner, created, end_date, starting_price, current_price) VALUES ("867ed72d-3fa9-4621-bfd0-3ca7b2b64075","tosh.jhomsson@lhuv.se","2022-05-07 08:09","2022-05-17 12:00",2500000,2500000);
INSERT INTO auction (uuid, owner, created, end_date, starting_price, current_price) VALUES ("18f1d168-01a7-4e06-953a-10d476d9091c","tosh.jhomsson@lhuv.se","2022-05-10 18:50","2022-05-21 12:00",15000000,15000000);


CREATE TABLE ongoing_auction (uuid TEXT PRIMARY KEY NOT NULL,
                              owner TEXT NOT NULL,
                              address TEXT NOT NULL,
                              square_meter INTEGER NOT NULL,
                              created TEXT NOT NULL,
                              end_date TEXT NOT NULL,
                              current_leader TEXT,
                              leading_bid INTEGER);



CREATE TABLE finished_auctions (uuid TEXT PRIMARY KEY NOT NULL,
                                owner TEXT NOT NULL,
                                winner TEXT NOT NULL,
                                winning_bid INTEGER NOT NULL,
                                total_winnings INTEGER NOT NULL);

CREATE TABLE bid_history (id INTEGER PRIMARY KEY AUTOINCREMENT,
                          auction_uuid TEXT NOT NULL,
                          bidder TEXT NOT NULL,
                          bid INTEGER NOT NULL,
                          bid_time DATETIME NOT NULL);

COMMIT;
