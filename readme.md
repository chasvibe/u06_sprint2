# u06 Private auction

## About the Project
Auction app that enables users to place bids on house listings with a twist.
https://docs.google.com/document/d/1GSJ8qVKwItfE1cFy3rqWujCpQ5l88O1YRSSOAgbag7g
For documentation in swedish.

### Built with
* Python 3.10
* Sqlite3

#### Dependencies
* Uuid
* Sqlite3
* Pytest
* Pylint
* autopep8
* Flake8

#### Usage
The app can be run in python with your favorite IDE, main.py.
Or in terminal, navigate to the project and launch it with "python3 main.py"

##### Files in this repository
* .gitlab-ci-yml
    -Codequality pipeline for Gitlab
* Schema_seeds_u06_database.sql
    -Template to create the database, use "cat Schema_seeds_u06_database.sql | sqlite3 dbdata.db"
     in terminal to easily recreate the database.
* dbdata.db
    -Database itself, make sure to name it "dbdata.db" for the app to function.
* main.py
    -The app file itself, contains all programlogic.
* readme.md
    -Hey, itsa me!
* requirements.txt
    -"pip install -r requirements.txt" can be used for virtualenv
* test_main.py
    -WIP upcoming tests

###### Sprint 1
* Setup Gitlab
    -prod and dev branch
    -.gitlab-ci.yml (Codequality pipeline)
* Setup database in Sqlite3
    -Schema and seeds
    -.db file
* Python app logic
    -main.py file to host the app
    -Register account
    -Login
    -View auctions
    -Exit program
    -Graph to showcase prices
    -complete auctions

###### Sprint 2
Priority 1.
* Complete the programlogic to reach MVP
    -Needs complete auctions with an end that divides the proceeds between the bidders.
* Dockerize the app in order to satisfy the teachers requirements.
* Unittests for main.py in order to satisfy the teachers requirements. (SCRAPED)

Priority 2.
* Graphs for showcase (SCRAPED)
* Send mail to auction winner
    -Mailhog can be used for testing

##### Running the app:
- Step one: Install dependencies from 'requirements.txt' either in a virtual env or directly.
- Step two: Run 'src/interface.py' from root project directory

#### Running with docker and docker compose (expecting docker and docker compose to already be installed):
## MacOS:
- Step one: Install xQuartz making sure you have homebrew installed (https://brew.sh/):
    __brew cask install xQuartz__
- Step Two: Run this command in your CLI to start xQuartz and create the connection between container and host:
    __xhost + <The IP of the machine>__
- Step three: Change the IP in the 'docker-compose.yml' file to the same as your local machine.
- Step four: Run this command to run docker: 
    __docker compose up --build__

## Not MacOS:
Step one: Run this command: 
    __docker compose up --build__
